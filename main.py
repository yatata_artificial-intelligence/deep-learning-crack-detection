import glob
import os
import numpy # linear algebra
import pandas # data processing, CSV file I/O (e.g. pd.read_csv)
import time
import matplotlib
import matplotlib.pyplot
import cv2
import PIL
import random
import sklearn
import sklearn.model_selection
import collections
import torch
import torchvision
import torchmetrics
import torchinfo
import datetime
import json
import tqdm







class DoubleConv(torch.nn.Module):
    def __init__(self, in_channels, out_channels):
        super(DoubleConv, self).__init__()
        self.conv = torch.nn.Sequential(
            torch.nn.Conv2d(in_channels, out_channels, 3, 1, 1, bias=False),
            torch.nn.BatchNorm2d(out_channels),
            torch.nn.ReLU(inplace=True),
            torch.nn.Conv2d(out_channels, out_channels, 3, 1, 1, bias=False),
            torch.nn.BatchNorm2d(out_channels),
            torch.nn.ReLU(inplace=True),
        )

    def forward(self, x):
        return self.conv(x)


class UNET(torch.nn.Module):

    def __init__(self, in_channels=3, out_channels=1, features=[64, 128, 256, 512]):

        super(UNET, self).__init__()

        self.name = "unet-batch_norm"

        self.ups = torch.nn.ModuleList()
        self.downs = torch.nn.ModuleList()
        self.pool = torch.nn.MaxPool2d(kernel_size=2, stride=2)

        # Down part of UNET
        for feature in features:
            self.downs.append(DoubleConv(in_channels, feature))
            in_channels = feature

        # Up part of UNET
        for feature in reversed(features):
            self.ups.append(
                torch.nn.ConvTranspose2d(
                    feature*2, feature, kernel_size=2, stride=2,
                )
            )
            self.ups.append(DoubleConv(feature*2, feature))

        self.bottleneck = DoubleConv(features[-1], features[-1]*2)
        self.final_conv = torch.nn.Conv2d(features[0], out_channels, kernel_size=1)

    def forward(self, x):
        skip_connections = []

        for down in self.downs:
            x = down(x)
            skip_connections.append(x)
            x = self.pool(x)

        x = self.bottleneck(x)
        skip_connections = skip_connections[::-1]

        for idx in range(0, len(self.ups), 2):
            x = self.ups[idx](x)
            skip_connection = skip_connections[idx//2]

            if x.shape != skip_connection.shape:
                x = torchvision.transforms.functional.resize(x, size=skip_connection.shape[2:])

            concat_skip = torch.cat((skip_connection, x), dim=1)
            x = self.ups[idx+1](concat_skip)

        return self.final_conv(x)



# Define the dataset class

class Dataset(torch.utils.data.Dataset):
	
    def __init__(self, data = None, data_directorys = None, image_directory = None, mask_directory = None,
                binarization_threshold = 0, transform = None):
        
        # store the image and mask filepaths, and augmentation
        # transforms
        super(Dataset, self).__init__()

        if (data_directorys != None):
            self.data_directorys = data_directorys

        else:

            self.image_directory = image_directory
            self.mask_directory = mask_directory
            self.image_paths = [os.path.join(image_directory, f) for f in os.listdir(image_directory)]
            self.mask_paths = [os.path.join(mask_directory, f) for f in os.listdir(mask_directory)]


            def match_files(dir1, dir2):
                files1 = [os.path.basename(f) for f in dir1]
                files2 = [os.path.basename(f) for f in dir2]
                matches = []
                #TODO check if all the files got a pair
                for file in set(files1).intersection(files2):
                    matches.append([os.path.join(dir1[files1.index(file)]), os.path.join(dir2[files2.index(file)])])
                return matches

            self.data_directorys = match_files(self.image_paths, self.mask_paths)
            '''
            self.data_directorys = []
            for i in range(len(self.image_paths)):
                self.data_directorys.append((self.image_paths[0], self.mask_paths[i]))
            '''
            '''
            self.data = []


            for i in range(len(self.image_paths)):
                imagePath = self.image_paths[i]
                maskPath = self.mask_paths[i]

                image = PIL.Image.open(imagePath).convert('RGB')
                mask = PIL.Image.open(maskPath).convert('L')

                # convert the PIL images to numpy arrays
                image = numpy.array(image)
                mask = numpy.array(mask)

                # normalize the image data to [0, 1] range
                #image = image / 255.

                self.data.append((image, mask))
            '''


        
        self.transform = transform
        self.binarization_threshold = binarization_threshold

    def __len__(self):
        # return the number of total samples contained in the dataset
        if (self.data_directorys != None):
            return(len(self.data_directorys))
        else:
            return len(self.image_paths)

    def __getitem__(self, idx):


        if (self.data_directorys != None):

            image, mask = self.data_directorys[idx]

            image = cv2.imread(image)
            image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
            mask = cv2.imread(mask, cv2.IMREAD_GRAYSCALE)

            #image = PIL.Image.fromarray(numpy.uint8(image)).convert('RGB')
            #mask = PIL.Image.fromarray(numpy.uint8(mask)).convert('L')

            image = numpy.uint8(image)
            mask = numpy.uint8(mask)
            
            
            

            # Binarize mask
            if (self.binarization_threshold > 0 and self.binarization_threshold <= 1):
                print("Mask Binarization")
                mask = numpy.where(mask >= self.binarization_threshold, 1, 0)

            sample = (image, mask)

            # Apply transformations
            if (self.transform != None):
            
                sample = self.transform(sample)


            


            return sample


        else:

            # grab the image path from the current index
            imagePath = self.image_paths[idx]
            # load the image from disk, swap its channels from BGR to RGB,
            # and read the associated mask from disk in grayscale mode
            image = cv2.imread(imagePath)
            image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
            mask = cv2.imread(self.mask_paths[idx], cv2.IMREAD_GRAYSCALE)
            
            image = PIL.Image.fromarray(numpy.uint8(image)).convert('RGB')
            mask = PIL.Image.fromarray(numpy.uint8(mask)).convert('L')

            sample = (image, mask)

 
            # Apply transformations
            if (self.transform != None):
                
                sample = self.transform(sample)

            return sample
        
        # Convert to PyTorch tensors
        
        # The transpose function in PyTorch is used to change
        # the dimension order of a tensor. In the case of an image,
        # the original order of dimensions is (height, width, channels),
        # but in PyTorch, the standard order is (channels, height, width).

        # Therefore, to convert an image to a PyTorch tensor, we need to
        # transpose the dimensions using the transpose function. The (2, 0, 1)
        # argument specifies the new order of dimensions. The first dimension,
        # which represents the channels in the image, is moved to the front,
        # and the other two dimensions are shifted back by one place.
        # This results in an image tensor with dimensions (channels, height, width),
        # which is the required format for input to most deep learning models in PyTorch.

        
        '''
        image = torch.from_numpy(image.transpose((2, 0, 1)))
        mask = torch.from_numpy(mask)
        mask = mask.unsqueeze(0)  # Add channel dimension
        '''
        #(image, mask) = tuple(sample.items())

        




class Model (torch.nn.Module):

    def __init__(self):
        
        super(Model, self).__init__()
        
        self.name = None

        self.channels_input = None
        self.channels_output = None
        self.blocks = torch.nn.ModuleList()
    
    def forward(self, X):
        
        if (self.name == "unet"):
            self.connections = []

            X = self.blocks[0].forward(X)
            self.connections.append(X)
            X = torch.nn.MaxPool2d(kernel_size = (2, 2), stride = 2, padding=0, dilation=1)(X)

            X = self.blocks[1].forward(X)
            self.connections.append(X)
            X = torch.nn.MaxPool2d(kernel_size = (2, 2), stride = 2, padding=0, dilation=1)(X)

            X = self.blocks[2].forward(X)
            self.connections.append(X)
            X = torch.nn.MaxPool2d(kernel_size = (2, 2), stride = 2, padding=0, dilation=1)(X)

            X = self.blocks[3].forward(X)
            self.connections.append(X)
            X = torch.nn.MaxPool2d(kernel_size = (2, 2), stride = 2, padding=0, dilation=1)(X)




            X = self.blocks[4].forward(X)
            
            
            if (X.shape != self.connections[-1].shape):
                X = torchvision.transforms.functional.resize(X, size = self.connections[-1].shape[2:])
            X = torch.cat((self.connections[-1], X), dim = 1)

            X = self.blocks[5].forward(X)

            if (X.shape != self.connections[-2].shape):
                X = torchvision.transforms.functional.resize(X, size = self.connections[-2].shape[2:])
            X = torch.cat((self.connections[-2], X), dim = 1)

            X = self.blocks[6].forward(X)
            
            if (X.shape != self.connections[-3].shape):
                X = torchvision.transforms.functional.resize(X, size = self.connections[-3].shape[2:])
            X = torch.cat((self.connections[-3], X), dim = 1)

            X = self.blocks[7].forward(X)

            if (X.shape != self.connections[-4].shape):
                X = torchvision.transforms.functional.resize(X, size = self.connections[-4].shape[2:])
            X = torch.cat((self.connections[-4], X), dim = 1)
            
            
            return self.blocks[8].forward(X)
    


class DoubleConvolutionBlock (torch.nn.Module):

    def __init__(self, channels_input:int, channels_output:int, mode, final_output = 1) -> None:
        
        super(DoubleConvolutionBlock, self).__init__()

        self.channels_input = channels_input
        self.channels_output = channels_output
        if (mode.lower() == "final"):
            self.final_output = final_output

        self.layers = torch.nn.ParameterList()

        self.build(mode)

    
    def build (self, mode):

        
        self.layers.append(torch.nn.Conv2d(in_channels = self.channels_input, 
                                    out_channels = self.channels_output, kernel_size = (3,3),
                                    stride = 1, padding = 1, dilation = 1, bias = True,
                                    padding_mode = "zeros", device=None))

        self.layers.append(torch.nn.ReLU())



        self.layers.append(torch.nn.Conv2d(in_channels = self.channels_output, 
                                    out_channels = self.channels_output, kernel_size = (3,3),
                                    stride = 1, padding = 1, dilation = 1, bias = True,
                                    padding_mode = "zeros", device=None))
        self.layers.append(torch.nn.ReLU())


        if (mode == "Down"):
            #layer3 = torch.nn.MaxPool2d(kernel_size = (2, 2), stride = 2, padding=0, dilation=1)
            #activation3 = None
            #self.layers.append(Layer(self.layer1, self.activation1))
            #self.layers.append(Layer(self.layer2, self.activation2))
            pass

        elif (mode == "Up"):

            self.layers.append(torch.nn.ConvTranspose2d(self.channels_output, (self.channels_output)//2,
                                kernel_size = (2, 2), stride=2, padding=0, dilation=1))
            #self.activation3 = None
            
            #self.layers.append(Layer(self.layer1, self.activation1))
            #self.layers.append(Layer(self.layer2, self.activation2))
            #self.layers.append(Layer(self.layer3, self.activation3))

        elif (mode == "Final"):

            self.layers.append(torch.nn.Conv2d(in_channels = self.channels_output, out_channels = self.final_output,
                                kernel_size = (1,1),stride = 1, padding = 0, dilation = 1, bias = True,
                                padding_mode = "zeros", device=None))          
            #self.activation3 = None

            #self.layers.append(Layer(self.layer1, self.activation1))
            #self.layers.append(Layer(self.layer2, self.activation2))
            #self.layers.append(Layer(self.layer3, self.activation3)) 

    def forward (self, X):

        for layer in self.layers:

            X = layer(X)
        
        return X

class Layer (torch.nn.Module):

    def __init__(self, layer, activationFunction) -> None:
        super(Layer, self).__init__()
    
        self.layer = layer
        self.activationFunction = activationFunction

    def forward (self, X):

        if self.activationFunction is not None:
            return self.activationFunction(self.layer(X))
        
        return self.layer(X)






class Function_Loss(torch.nn.Module):

    def __init__(self, loss_type, problem, problem_type, device):
        super(Function_Loss, self).__init__()

        #standard values
        self.problem = "segmentation"
        self.problem_type = "supervised"
        self.loss_type = None
        self.loss_dict = None
        self.device = device

        self.__parameter_checker(loss_type = loss_type, problem = problem, problem_type = problem_type)



        if (self.problem == "segmentation"):
            self.loss_dict = {
                "accuracy_pixel": {
                    "loss": None,
                    "average": None,
                    "std": None
                },
                "binary_cross_entropy": {
                    "loss": None,
                    "average": None,
                    "std": None
                },
                "dice_coefficient": {
                    "loss": None,
                    "average": None,
                    "std": None
                },
                "iou": {
                    "loss": None,
                    "average": None,
                    "std": None
                },
                "focal": {
                    "loss": None,
                    "average": None,
                    "std": None
                }
            }



    def forward(self, y_pred, y_true):

    
        loss = None
        avg_loss = None
        std_loss = None

        #regression function loss
        if (self.problem == "regression"):


            # mae
                #self.function_loss = torch.abs(y_pred - y_true)
            # mse

            # rmse

            return
        # classification

        elif (self.problem == "classification"):
            return
        

        # segmentation function loss
        elif(self.problem == "segmentation"):
            
            
            smooth=1e-5


            # accuracy pixel loss

            def accuracy_pixel_loss(Y_pred, Y_true):
                '''
                Computes the accuracy of pixels between two batches of binary masks
                '''

                Y_pred_sig = torch.sigmoid(Y_pred)
                Y_pred_thresh = torch.zeros_like(Y_pred_sig)
                Y_pred_thresh[Y_pred_sig > 0.5] = 1

                # normalize the predictions and targets
                #Y_pred_norm = (Y_pred_aux - torch.mean(Y_pred_aux, axis=(1, 2, 3), keepdim=True)) / torch.std(Y_pred_aux, axis=(1, 2, 3), keepdim=True)
                #Y_true_norm = (Y_true - torch.mean(Y_true, axis=(1, 2, 3), keepdim=True)) / torch.std(Y_true, axis=(1, 2, 3), keepdim=True)

                # flatten predictions and targets
                Y_pred_flat = Y_pred_thresh.view(Y_pred_thresh.shape[0], -1)
                Y_true_flat = Y_true.view(Y_true.shape[0], -1)

                # calculate the number of correct predictions for each batch element
                correct = torch.sum(Y_pred_flat.to(torch.uint8) == Y_true_flat.to(torch.uint8), axis=1).float()

                # calculate the total number of predictions for each batch element
                total = Y_pred_flat.shape[1]

                # calculate the accuracy for each batch element
                accuracy_pixel_loss = torch.zeros(Y_pred_thresh.shape[0])
                for i in range(Y_pred_thresh.shape[0]):
                    accuracy_pixel_loss[i] = 1.0 - (correct[i] / total)

                return accuracy_pixel_loss

            
            loss = accuracy_pixel_loss(y_pred.float(), y_true.float())

            self.loss_dict.get("accuracy_pixel").update({"loss": loss})
            self.loss_dict.get("accuracy_pixel").update({"average": torch.mean(loss)})
            self.loss_dict.get("accuracy_pixel").update({"std": torch.std(loss)})
            self.loss_dict.get("accuracy_pixel").update({"score_std": torch.std(torch.sub(1,loss))})


            # binary cross entropy with logistic loss

            function_loss = torch.nn.BCEWithLogitsLoss(reduction = "none")

            loss = function_loss(y_pred.float(), y_true.float())
            self.loss_dict.get("binary_cross_entropy").update({"loss": loss})
            self.loss_dict.get("binary_cross_entropy").update({"average": torch.mean(loss)})
            self.loss_dict.get("binary_cross_entropy").update({"std": torch.std(loss)})


            # dice_coefficient loss
            
            def dice_and_iou_loss (Y_pred, Y_true):

                
 
                Y_pred_sig = torch.sigmoid(Y_pred)
                
                #Y_pred_thresh = torch.zeros_like(Y_pred_sig)
                #Y_pred_thresh[Y_pred_sig > 0.5] = 1.0


                if (torch.isnan(Y_pred_sig).any()):
                    print("Y_pred_sig has NaN")

                if (torch.isnan(Y_true).any()):
                    print("Y_true has NaN")


                # normalize the predictions and targets
                #Y_pred_norm = (Y_pred_aux - torch.mean(Y_pred_aux)) / torch.std(Y_pred_aux)
                #Y_true_norm = (Y_true - torch.mean(Y_true)) / torch.std(Y_true)



                # flatten predictions and targets
                #Y_pred_flat = Y_pred_norm.view(-1)
                #Y_true_flat = Y_true_norm.view(-1)

                # flattening the values makes it more sensible to 
                # pixel by pixel differences while
                # not flat calculates score by whole image

                #Y_pred_flat = Y_pred_norm
                #Y_true_flat = Y_true_norm
                
                # calculate the intersection and union for each sample in the batch
                intersection = (Y_pred_sig * Y_true).sum(dim=(1, 2, 3))
                union = Y_pred_sig.sum(dim=(1, 2, 3)) + Y_true.sum(dim=(1, 2, 3))

                # calculate the dice score and loss for each sample in the batch
                dice_score = (2.00 * intersection + smooth) / (union + smooth)

                
                # calculate the IoU score and loss for each sample in the batch
                #iou_score = (intersection + smooth) / (union + smooth)
                

                #iou_score = torchmetrics.functional.jaccard_index(Y_pred_sig, Y_true, reduction='none')

                jaccard = torchmetrics.JaccardIndex(task="binary", threshold= 0.5).to(self.device)
                #print(Y_true)
                iou_score = jaccard(Y_pred_sig, Y_true.to(torch.uint8))
                # check if mask true is completely black or white
                '''
                for i in range(Y_true.size(0)):

                    # mask true is all black
                    if (Y_true[i].sum() == 0):


                        if (Y_pred_sig[i].sum() == 0):  

                            dice_score[i] = 1.0
                            iou_score[i] = 1.0

                        else:
                            
                            #Morphologic Operation
                            mask = Y_pred_sig[i].clone()
                            # Define the kernel size for the operation
                            dilate_kernel_size = 3

                            # Create a structuring element for the kernel
                            dilate_kernel = torch.ones(1, 1, dilate_kernel_size, dilate_kernel_size, dtype=torch.float)

                            # Dilate and Erode the mask tensor
                            dilated_tensor = torch.nn.functional.conv2d(mask.float().unsqueeze(0), dilate_kernel.to("cuda"), padding=dilate_kernel_size//2)
                            #eroded_tensor = torch.nn.functional.conv2d(dilated_tensor, closing_kernel, padding=closing_kernel_size//2)

                            dilated_tensor = torch.nn.functional.conv2d(dilated_tensor, dilate_kernel.to("cuda"), padding=dilate_kernel_size//2)
                            
                            mask_final = dilated_tensor
                            mask_final_sum = mask_final.sum()
                        

                            #dice_score[i] = 0.7 - float(mask_final_sum/(mask_final.size()[0]*mask_final.size()[1]))
                            #iou_score[i] = 0.7 - float(mask_final_sum/(mask_final.size()[0]*mask_final.size()[1]))
                            
                            dice_score[i] = torch.median(dice_score)
                            iou_score[i] = torch.median(iou_score)
                        
                    
               
                    
                    # mask true is all white
                    elif (Y_true[i].sum() == Y_true[i].numel()):

                        if (Y_pred_sig[i].sum() == Y_pred_sig[i].numel()):

                            dice_score[i] = 1.0
                    
                        elif (dice_score[i] == 0.0):

                            # TODO predicted mask is not totatly white but might be close
                            dice_score[i] = 1.0
                        # TODO predicted mask is not totatly black but might be close
                        dice_score[i] = 1.0 - float(Y_pred_sig[i].sum()/(255*255))
                    
                    '''
                
                    # When both masks are completely black, the intersection between them will also be black,
                    # and the union will be equal to the sum of the number of pixels in each mask. 
                    # Therefore, the Dice score will be 0, indicating no overlap between the masks, 
                    # and the Dice loss will be 1, indicating the maximum loss.

                    # It is important to note that the Dice score and loss are only meaningful when
                    # both masks have at least some non-zero pixels, as they are designed to measure the similarity
                    # between two binary masks. If both masks are completely white 
                    # (i.e., all pixels have a value of 1), then the Dice score and loss will also be meaningless,
                    #  as there will be no difference between the predicted and true masks.
                
                

                loss_dice = 1.00 - dice_score
                loss_iou = 1.00 - iou_score

                if (torch.isnan(loss_dice).any()):
                    print("Dice Coefficient Loss has NaN")
                    print(loss_dice)

                elif (torch.isnan(loss_iou).any()):
                    print("IoU Loss has NaN")
                    print(loss_iou)


                return loss_dice, loss_iou
    
           
            loss_dice, loss_iou = dice_and_iou_loss(y_pred.float(), y_true.float())

            self.loss_dict.get("dice_coefficient").update({"loss": loss_dice})
            self.loss_dict.get("dice_coefficient").update({"average": torch.mean(loss_dice)})
            self.loss_dict.get("dice_coefficient").update({"std": torch.std(loss_dice)})
            self.loss_dict.get("dice_coefficient").update({"score_std": torch.std(torch.sub(1,loss_dice))})


            #iou

            self.loss_dict.get("iou").update({"loss": loss_iou})
            self.loss_dict.get("iou").update({"average": torch.mean(loss_iou)})
            self.loss_dict.get("iou").update({"std": torch.std(loss_iou)})



            # focal loss


            def focal_loss(Y_pred, Y_true, gamma=2.0, alpha=0.25):



                if (torch.isnan(Y_pred).any()):
                    print("Y_pred has NaN")

                if (torch.isnan(Y_true).any()):
                    print("Y_true has NaN")

                # Calculate binary cross-entropy loss
                function_loss = torch.nn.BCEWithLogitsLoss(reduction = "none")

                bce_loss = function_loss(y_pred.float(), y_true.float())
                #bce_loss = torch.nn.functional.binary_cross_entropy_with_logits(Y_pred, Y_true, reduction='none')

                # Calculate focal loss weights
                pt = torch.exp(-bce_loss)
                focal_weights = alpha * (1 - pt) ** gamma

                # Apply focal weights to the binary cross-entropy loss
                focal_bce_loss = focal_weights * bce_loss

                return focal_bce_loss
          
            loss_focal = focal_loss(y_pred.float(), y_true.float())
            self.loss_dict.get("focal").update({"loss": loss_focal})
            self.loss_dict.get("focal").update({"average": torch.mean(loss_focal)})
            self.loss_dict.get("focal").update({"std": torch.std(loss_focal)})


        loss = self.loss_dict.get(self.loss_type).get("loss")
        avg_loss = self.loss_dict.get(self.loss_type).get("average")
        std_loss = self.loss_dict.get(self.loss_type).get("std")


        return loss, avg_loss, std_loss


    def __parameter_checker(self, loss_type, problem, problem_type):


        if(loss_type != None):
        
            loss_type = loss_type.lower()

            if (loss_type == "mae" or loss_type == "mse" or 
            
                loss_type == "iou" or loss_type == "binary_cross_entropy" or
                loss_type == "binary_cross_entropy_with_logistic_loss" or loss_type == "dice_coefficient" or
                loss_type == "binary_cross_entropy_with_dice_loss"):

                self.loss_type = loss_type
                

        if(problem != None):
            
            problem = problem.lower()
            if (problem == "regression" or problem == "classification" or
                problem == "detection" or problem == "segmentation" or
                problem == "anomaly" or problem == "mix"):
            
                self.problem = problem
            else:
                print("ERROR -> Function Loss Paramater Checker: Problem")
        
                
        if(problem_type != None):
            
            problem_type = problem_type.lower()
            if (problem_type == "supervised" or problem_type == "unsupervised" or
                problem_type == "reinforcement" or problem_type == "semi-supervised" or
                problem_type == "transfer" or problem_type == "active" or 
                problem_type == "generative" or problem_type == "recommendation"):
            
                self.problem_type = problem_type
            else:
                print("ERROR -> Function Loss Paramater Checker: Problem Type")

   







class Iteration():
        
    
    def __init__(self, problem, problem_type, name = None, debugMode = True,
                 data_type = None, workingDirectory = None,
                 transform = None, seed = None):
        
    
        #standard parameters
        self.name = None

        self.data = None
        self.data_directory = None
        self.data_type = None
        
        self.data_train = None
        self.data_test = None

        self.model = Model()
        self.model_built = False
        self.model_trained = False
        self.transferLearning = False

        self.batch_size = 16
        self.function_loss = None
        self.optimizer = None

        self.problem_type = None
        self.problem = None

        self.setup_mark = False
        self.statistics_train_batch = []
        self.statistics_train_epoch = []
        self.statistics_train_fold = []

        self.statistics_validation_epoch = []
        self.statistics_validation_fold = []

        self.statistics_test = []


        self.debugMode = True
        self.device = "cpu"

        self.workingDirectory = os.getcwd()
        self.resultsDirectory = None
        self.configDirectory = None

        self.config = {
            
            "datetime": None,

            "data": {
                "loadingTime": None,
                "transformations": None,
                "train_size": None,
                "test_size": None,
                "test_factor": None
            },
            

            "model": {
                "input": {
                    "image_size" : None,
                    "image_channels" : None
                },
                "output": {
                    "image_size" : None,
                    "image_channels" : None
                },
                "name": None,
            },

            "train": {

                "batch_size": self.batch_size,
                "n_epochs": None,
                "kFolds": None,
                "function_loss": None,

                "optimizer":{
                    "name": None,
                    "learningRate": None,
                    "momentum": None
                } ,
                "trainingTime": None,
                "trainingTime_epoch": []
            },

            "statistics":{
                "train_results_batch": None,
                "train_results_epoch": None,
                "train_results_fold": None,

                "validation_results_fold": None,

                "test_results": None
            },

            "others":{
                "problem": None,
                "problem_type": None,
                "data_type": None,
                "device": None,
                "version": 1.5
            }
        }



        self.__parameter_checker(name = name, problem = problem, problem_type = problem_type, data_type = data_type, workingDirectory = workingDirectory) 
        
        self.__updateConfig(problem = self.problem, problem_type = self.problem_type, data_type = self.data_type)
    
        if (self.debugMode == True):
            print("V{0}".format(self.config.get("others").get("version")))
      





    #Setup Function
    ####################################################################################
    def setup(self, data = None, data_test = None, test_factor = 0.2, model = None,
                channels_input = 3, channels_output = 3, image_size = 256, device = "cpu"):
        


        if (self.debugMode == True):
            print("Setup: Initialization")


        if (isinstance(model, str)):
            model_name = model
            self.transferLearning = False
        else:
            self.transferLearning = True
            model_name = "Transfer_Learning_Model"


        self.__parameter_checker(data = data, model = model_name, test_factor = test_factor,
                                channels_input = channels_input, channels_output = channels_output,
                                device = device)



        self.__updateConfig(model_name = model_name, test_factor = test_factor, device = device,
                            image_input_channels = channels_input, image_output_channels = channels_output,
                            image_input_size = image_size, image_output_size = image_size)



       
        print("-> Iteration of: {0}, {1}, of {2} Data".format(self.problem_type, self.problem, self.data_type))

        
        


        self.__setupDirectory()
        self.__data_setup(data, test_factor, image_size = image_size)
        self.__model_setup(model)
    



        self.setup_mark = True

        if (self.debugMode == True):
            print("Setup: Complete")
    

    def __setupDirectory(self):

        if (self.name == None):

            # get the current date and time
            current_time = datetime.datetime.now()

            # format the current date and time to "dd_mm_yyyy hh:mm:ss"
            self.name = current_time.strftime("%d_%m_%Y-%H:%M:%S")


        #self.workingDirectory = os.path.join(self.workingDirectory, self.name)



        print("-> Iteration Name: {0}".format(self.name))
        print("-> Working Directory: {}".format(self.workingDirectory))

        # Define the path of the new directory
        self.resultsDirectory = os.path.join(self.workingDirectory, "results")
        self.configDirectory = os.path.join(self.workingDirectory, "config")


        #self.resultsDirectory = self.workingDirectory
        #self.configDirectory = self.workingDirectory


        # Use the os.makedirs() function to create the new directory
        if (os.path.exists(self.resultsDirectory) == False):
            os.makedirs(self.resultsDirectory)

        if (os.path.exists(self.configDirectory) == False):
            os.makedirs(self.configDirectory)


        print("--> Results Directory: {}".format(self.resultsDirectory))
        print("--> Config Directory: {}".format(self.configDirectory))





    #Analisys Section
    #########################################################################################
    def __parameter_checker(self, name = None, data = None, model = None, data_type = None, test_factor = None,
                          channels_input = None, channels_output = None, problem = None, problem_type = None,
                          workingDirectory = None, device = None, debugMode = None):
        

        if (name != None):
            self.name = name

        if (data != None):
            pass
        
        if (model != None):
            
            model = model.lower()
            if (model == "unet"):
                self.model_name = model
            elif (model == "unet-batch_norm"):
                self.model_name = model
            elif(model == "transfer_learning_model"):
                self.model_name = model
            else:
                print("ERROR -> Paramater Checker: Model")
        
        if (channels_input != None):
            if (isinstance(channels_input, int) == True):
                self.model_channels_input = channels_input
            else:
                print("ERROR -> Paramater Checker: Channels Input")

        if (channels_output != None):
            if (isinstance(channels_output, int) == True):
                self.model_channels_output = channels_output
            else:
                print("X ERROR -> Paramater Checker: Channels Output")


        if (test_factor != None):
            self.test_factor = test_factor
            if (test_factor > 1.0 or test_factor < 0.0):
                print("X ERROR -> Paramater Checker: Model")


        if (data_type != None):
        
            data_type.lower()
            if (data_type == "tabular" or data_type == "image" or
                data_type == "audio" or data_type == "video" or
                data_type == "mix"):
        
                self.data_type = data_type
            else:
                print("ERROR -> Paramater Checker: Data Type")
        
        if(problem != None):
            
            problem = problem.lower()
            if (problem == "regression" or problem == "classification" or
                problem == "detection" or problem == "segmentation" or
                problem == "anomaly" or problem == "mix"):
            
                self.problem = problem
            else:
                print("ERROR -> Paramater Checker: Problem")
        
        
        
        
        if(problem_type != None):
            
            problem_type = problem_type.lower()
            if (problem_type == "supervised" or problem_type == "unsupervised" or
                problem_type == "reinforcement" or problem_type == "semi-supervised" or
                problem_type == "transfer" or problem_type == "active" or 
                problem_type == "generative" or problem_type == "recommendation"):
            
                self.problem_type = problem_type
            else:
                print("ERROR -> Paramater Checker: Problem Type")


        if (workingDirectory != None):

            if os.path.exists(workingDirectory):
                self.workingDirectory = workingDirectory
            else:
                print("X ERROR: Path does not exist.")


        if (device != None):
            
            if (device == "gpu"):

                if (torch.cuda.is_available()):
                    self.device = torch.device("cuda:0")
                else:
                    print("X ERROR -> Device Not Available. Using CPU")
                    self.device = torch.device("cpu")

            elif(device == "cpu"):
                self.device = torch.device("cpu")
            else:
                print("X ERROR -> Paramater Checker: Device")


        if (debugMode != None):
            if(debugMode == True or debugMode == False):
                self.debugMode = debugMode
      

    def analysis_pre(self, image_size = 256, plot_image_resolution = False):

        if (self.debugMode == True):
            print("Analysis Mode: Initialization")


        if(self.setup_mark == False):
            print("ERROR -> Analysis Pre: Setup Mark")
            return
        self.__analysis_data(plot_image_resolution = plot_image_resolution)
        self.__analysis_model(image_size = image_size)


        if (self.debugMode == True):
            print("Analysis Mode: Complete")


    def __analysis_data(self, n_train_sampler = 10, n_test_sampler = 10, plot_image_resolution = False):
        
        
        if(self.debugMode == True):
            print("---------------------------------------------------------------------------------")
            print("-> Analysis Data: Initialization")


        
        if (plot_image_resolution == True):

            def plot_image_sizes(dataset, num_samples=1000):
                """Plots a bar plot of the most used image sizes returned by the dataset's __getitem__ method."""
                
                # Collect the image sizes in a dictionary
                sizes = collections.defaultdict(int)
                for i in range(num_samples):
                    img, mask = self.__get_custom_sample(dataset, i, original = True)
                    img = numpy.array(img)
                    sizes[img.shape] += 1
                
                # Sort the dictionary by count and convert it to a list of tuples
                sorted_sizes = sorted(sizes.items(), key=lambda x: x[1], reverse=True)
                
                # Extract the size and count from each tuple
                labels, values = zip(*sorted_sizes)
                
                # Plot the bar chart
                matplotlib.pyplot.bar(range(len(labels)), values)
                matplotlib.pyplot.xticks(range(len(labels)), labels)
                matplotlib.pyplot.xlabel("Image Size")
                matplotlib.pyplot.ylabel("Frequency")
                matplotlib.pyplot.title("Image Size Distribution")
                matplotlib.pyplot.show()


            plot_image_sizes(self.data, num_samples = self.data.__len__())

        self.__train_test_analysis(n_train_sampler = n_train_sampler, n_test_sampler = n_test_sampler)
    


        
        if(self.debugMode == True):
            print("-> Analysis Data: Complete")


    def __analysis_model(self, image_size = None):


        if (image_size == None):
            image_size = self.config.get("model").get("input").get("image_size")


        if(self.debugMode == True):
            print("---------------------------------------------------------------------------------")
            print("-> Analysis Model: Initialization")


        # Check if the model has trainable parameters
        params = list(self.model.parameters())

        if(self.debugMode == True):
            print("--> Model Trainable Parameters: {0}".format(len(params)))

        # Model Layers
        print(torchinfo.summary(self.model, input_size=(1, self.model_channels_input, image_size, image_size), depth = 20))



        # print the requires_grad attribute for each layer
        print("Model Learnable Layers (Require Gradient):")
        for name, param in self.model.named_parameters():
            print('{}: {}'.format(name, param.requires_grad))

        


        # Unet Max Level for Image Size
        if ("unet" in self.config.get("model").get("name")):
        
            print("For Input Image Size: {0}".format(image_size))
            def compute_max_depth(shape, max_depth=20, print_out=True):
                
                shapes = []
                shapes.append(shape)

                for level in range(1, max_depth):

                    if shape % 2 ** level == 0 and shape / 2 ** level > 1:
                        shapes.append(shape / 2 ** level)
                        if print_out:
                            print(f'Level {level}: {shape / 2 ** level}')
                    else:
                        if print_out:
                            print(f'Max-level: {level - 1}')
                        break

                return shapes

            print(compute_max_depth(image_size, print_out=True))




        self.__model_debug(image_size = image_size)


        if(self.debugMode == True):
            print("-> Analysis Model: Complete")


    def __train_test_analysis(self, n_train_sampler, n_test_sampler):



        # Train/Test Pie Chart
        fig, axes = matplotlib.pyplot.subplots(1,1, figsize = (4,4))
        labels = "Train Data", "Test Data"
        sizes = [len(self.data_train), (len(self.data_test))]
       
        def labeling_func(size):
            relative = size/(len(self.data_train) + (len(self.data_test)))
            return "{0}\n{1:.2f}%".format(size, relative)
            
        axes.pie(sizes, labels = labels)
        #TODO put sizes absolute value in pie chart


        #wedges, texts, autotexts = axes.pie(sizes, labels=labels, autopct = lambda size: labeling_func(size))
        axes.set_title("Train/Test Chart with Test Factor: {0}".format(self.test_factor))
        #axes.legend(wedges, labels, title = "Data", loc = "best")


        # Train and Test Sampler
        if (self.data_type == "image"):

            if (self.problem_type == "supervised"):

                fig, axes = matplotlib.pyplot.subplots(n_train_sampler, 4, figsize = (3*4, 4*n_train_sampler))

                for i in range(0, n_train_sampler, 1):

                    index = random.randint(0, self.data_train.__len__())

                    sample = list(self.data_train.__getitem__(index))

                    # Assume image_tensor is a PyTorch tensor object with shape (C, H, W)
                    # Swap the order of dimensions to be (H, W, C) and convert to a NumPy array
                    sample[0] = sample[0].permute(1, 2, 0).numpy()
                    sample[1] = sample[1].permute(1, 2, 0).numpy()


                    original = list(self.__get_custom_sample(data = self.data_train, index = index, original = True))

                    axes[i][0].imshow(original[0])
                    axes[i][0].set_title("Original Sample: {0}\n Size {1}".format(index, original[0].size))

                    axes[i][1].imshow(original[1], cmap = "gray")
                    axes[i][1].set_title("Original Mask: {0}\n Size {1}".format(index, original[0].size))

                    
                    if (self.model_channels_input == 1):
                        axes[i][2].imshow(sample[0], cmap = "gray")
                    else:
                        axes[i][2].imshow(sample[0])
                    axes[i][2].set_title("Train X: {0}\n Size {1}".format(index, sample[0].shape))

                    axes[i][3].imshow(sample[1], cmap = "gray")
                    axes[i][3].set_title("Train Y: {0}\n Size {1}".format(index, sample[1].shape))
                

                fig, axes = matplotlib.pyplot.subplots(n_test_sampler, 4, figsize = (3*4,4*n_train_sampler))

                for i in range(0, n_test_sampler, 1):

                    index = random.randint(0, self.data_test.__len__())
                    sample = list(self.data_test.__getitem__(index))

                    # Assume image_tensor is a PyTorch tensor object with shape (C, H, W)
                    # Swap the order of dimensions to be (H, W, C) and convert to a NumPy array
                    sample[0] = sample[0].permute(1, 2, 0).numpy()
                    sample[1] = sample[1].permute(1, 2, 0).numpy()


                    original = list(self.__get_custom_sample(data = self.data_test, index = index, original = True))

                    axes[i][0].imshow(original[0])
                    axes[i][0].set_title("Original Sample: {0}\n Size {1}".format(index, original[0].size))

                    axes[i][1].imshow(original[1], cmap = "gray")
                    axes[i][1].set_title("Original Mask: {0}\n Size {1}".format(index, original[0].size))

                    if (self.model_channels_input == 1):
                        axes[i][2].imshow(sample[0], cmap = "gray")
                    else:
                        axes[i][2].imshow(sample[0])
                    axes[i][2].set_title("Test X: {0}\n Size {1}".format(index, sample[0].shape))

                    axes[i][3].imshow(sample[1], cmap = "gray")
                    axes[i][3].set_title("Test Y: {0}\n Size {1}".format(index, sample[1].shape))




                
                fig, axes = matplotlib.pyplot.subplots(2, 2, figsize = (10, 12))

                #index = random.randint(0, self.data_test.__len__())
                index = 20


                original = list(self.__get_custom_sample(data = self.data_test, index = index, original = True))
                original[0] = numpy.array(original[0])
                original[1] = numpy.array(original[1])

                fig.suptitle("Pixel Distribution")
                axes[0][0].hist(original[0].ravel(), bins=80, density=True)
                axes[0][0].set_xlabel("Pixel Intensity")
                axes[0][0].set_ylabel("Frequency Relative")
                axes[0][0].set_title("Original Image: {0}".format(index))


                sample = list(self.data_test.__getitem__(index))
                sample[0] = sample[0].permute(1, 2, 0).numpy()
                sample[1] = sample[1].permute(1, 2, 0).numpy()


                axes[0][1].hist(sample[0].ravel(), bins=80, density=True)
                axes[0][1].set_xlabel("Pixel Intensity")
                axes[0][1].set_ylabel("Frequency Relative")
                axes[0][1].set_title("Image After Pre-Processing: {0}".format(index))




                axes[1][0].hist(original[1].flatten(), bins=80, density=True)
                axes[1][0].set_xlabel("Pixel Intensity")
                axes[1][0].set_ylabel("Frequency Relative")
                axes[1][0].set_title("Original Mask: {0}".format(index))


                axes[1][1].hist(sample[1].flatten(), bins=80, density=True)
                axes[1][1].set_xlabel("Pixel Intensity")
                axes[1][1].set_ylabel("Frequency Relative")
                axes[1][1].set_title("Mask After Pre-Processing: {0}".format(index))
        

    def analysis_pos(self, n_predictions = 30, debug_path = None):

    
        self.model.eval()

        if (self.debugMode == True):

            print("Model Evaluation: {0}".format(self.model.name))



        if (self.model_trained != True):
            print("X ERROR -> Model Evaluation: Model Not Trained")
            return

                            
            
        

        
        
        # Plot Learning Curves
        ##################################################################################
        
        # if regression
            # R2 score
        
        # if classifier
            # Confusion Matrix training data
            # Confusion Matrix test data
            # COnsequentemnte, precioson, recal e f1 score para trianing e test
            # ROC CUrver
            # Mean Average Precision (mAP): This metric is commonly used to evaluate object detection models, but can also be used for image segmentation problems. It calculates the precision and recall values for different IoU thresholds, and then calculates the average precision across all IoU thresholds.

        # if segmentation
            # pixel accuracy
            # IoU
            # Dice
        
        
        
        
        
        # Performance by Batch
        ######################################################################
        # self.statistics_train_batch -> [data_number_total, performance]
                
        
        fig, axes = matplotlib.pyplot.subplots(2, 3, figsize = (6*3, 7*2))
        
        fig.suptitle("Performance by Batch")
        
        
        
        #X = numpy.arange(1, len(self.statistics_train_batch)+1, 1) * self.batch_size
        
    
        # statistics_train_batch.append([data_number_total, avg_loss, std_loss, performance])
        #train_batch_array = numpy.array([[item[0], item[1], item[2]] for item in self.statistics_train_batch])


        X = numpy.array([item[0] for item in self.statistics_train_batch])
        performance_train_batch = numpy.array([item[-1] for item in self.statistics_train_batch])


        train_batch_accuracy_average = numpy.array([performance.get("accuracy_pixel").get("average") for performance in performance_train_batch])
        #train_batch_accuracy_std = numpy.array([performance.get("accuracy_pixel").get("std") for performance in performance_train_batch])

        train_batch_diceCoefficient_average = numpy.array([performance.get("dice_coefficient").get("average") for performance in performance_train_batch])
        #train_batch_diceCoefficient_std = numpy.array([performance.get("dice_coefficient").get("std") for performance in performance_train_batch])

        train_batch_iou_average = numpy.array([performance.get("iou").get("average") for performance in performance_train_batch])
        #train_batch_iou_std = numpy.array([performance.get("iou").get("std") for performance in performance_train_batch])

        train_batch_bce_average = numpy.array([performance.get("binary_cross_entropy").get("average") for performance in performance_train_batch])
        #train_batch_bce_std = numpy.array([performance.get("binary_cross_entropy").get("std") for performance in performance_train_batch])

        train_batch_focal_average = numpy.array([performance.get("focal").get("average") for performance in performance_train_batch])
        #train_batch_focal_std = numpy.array([performance.get("focal").get("std") for performance in performance_train_batch])




        axes[0][0].plot(X, train_batch_accuracy_average, "-", color = "r")
        axes[0][0].set_title("Accuracy Score")
        axes[0][0].set_xlabel("Number of Samples")
        axes[0][0].set_ylabel("Score")
        axes[0][0].legend()
        axes[0][0].grid()

        
        axes[0][1].plot(X, train_batch_diceCoefficient_average, "-", color = "r")
        axes[0][1].set_title("Dice Coefficient Loss")
        axes[0][1].set_xlabel("Number of Samples")
        axes[0][1].set_ylabel("Loss")
        axes[0][1].legend()
        axes[0][1].grid()

        axes[0][2].plot(X, train_batch_iou_average, "-", color = "r")
        axes[0][2].set_title("Intersection over Union Loss")
        axes[0][2].set_xlabel("Number of Samples")
        axes[0][2].set_ylabel("Loss")
        axes[0][2].legend()
        axes[0][2].grid()
 

        axes[1][0].plot(X, train_batch_bce_average, "-", color = "r")
        axes[1][0].set_title("Binary Cross-Entropy Loss")
        axes[1][0].set_xlabel("Number of Samples")
        axes[1][0].set_ylabel("Loss")
        axes[1][0].legend()
        axes[1][0].grid()
    
        
        
        axes[1][1].plot(X, train_batch_focal_average, "-", color = "r")
        axes[1][1].set_title("Focal Loss")
        axes[1][1].set_xlabel("Number of Samples")
        axes[1][1].set_ylabel("Loss")
        axes[1][1].legend()
        axes[1][1].grid()
        

        
        matplotlib.pyplot.savefig("analysis_performance_batch.png")
        #matplotlib.pyplot.savefig(self.resultsDirectory + "/" + "analysis_performance_batch.png")
        
        


        
        # Performance by Epoch
        ######################################################################
        # self.statistics_train_epoch -> [n_epochs_total, epoch_performance]
        
        fig, axes = matplotlib.pyplot.subplots(2, 3, figsize = (6*3, 7*2))
        
        
        fig.suptitle("Performance by Epoch")
        
        
        
        #X = numpy.arange(1, self.n_epochs * self.KFolds + 1, 1)
        X = numpy.array([item[0] for item in self.statistics_train_epoch])

        performance_train_epoch = numpy.array([item[-1] for item in self.statistics_train_epoch])
        performance_validation_epoch = numpy.array([item[-1] for item in self.statistics_validation_epoch])

        
        #train_epoch_array = numpy.array([[item[0], item[1], item[2]] for item in self.statistics_train_epoch])
        #validation_epoch_array = numpy.array([[item[0], item[1], item[2]] for item in self.statistics_validation_epoch])
        


        train_epoch_accuracy_average = numpy.array([performance.get("accuracy_pixel").get("average") for performance in performance_train_epoch])
        train_epoch_accuracy_std = numpy.array([performance.get("accuracy_pixel").get("average") for performance in performance_train_epoch])    

        validation_epoch_accuracy_average = numpy.array([performance.get("accuracy_pixel").get("average") for performance in performance_validation_epoch])
        validation_epoch_accuracy_std = numpy.array([performance.get("accuracy_pixel").get("std") for performance in performance_validation_epoch])
        
            
        axes[0][0].plot(X, train_epoch_accuracy_average, "-o", color = "r", label = "Training Score")
        axes[0][0].plot(X, validation_epoch_accuracy_average, "-o", color = "g", label = "Validation Score")
        
        axes[0][0].fill_between(X, train_epoch_accuracy_average - train_epoch_accuracy_std,
                                train_epoch_accuracy_average + train_epoch_accuracy_std,
                                color = "r", alpha = 0.1)
        axes[0][0].fill_between(X, validation_epoch_accuracy_average - validation_epoch_accuracy_std,
                                validation_epoch_accuracy_average + validation_epoch_accuracy_std,
                                color = "g", alpha = 0.1)
        
        axes[0][0].set_title("Accuracy Score")
        axes[0][0].set_xlabel("Epoch")
        axes[0][0].set_ylabel("Score")
        axes[0][0].legend()
        axes[0][0].grid()




        train_epoch_diceCoefficient_average = numpy.array([performance.get("dice_coefficient").get("average") for performance in performance_train_epoch])
        train_epoch_diceCoefficient_std = numpy.array([performance.get("dice_coefficient").get("std") for performance in performance_train_epoch])
        
        validation_epoch_diceCoefficient_average = numpy.array([performance.get("dice_coefficient").get("average") for performance in performance_validation_epoch])
        validation_epoch_diceCoefficient_std = numpy.array([performance.get("dice_coefficient").get("std") for performance in performance_validation_epoch])


        axes[0][1].plot(X, train_epoch_diceCoefficient_average, "-o", color = "r", label = "Training Loss")
        axes[0][1].plot(X, validation_epoch_diceCoefficient_average, "-o", color = "g", label = "Validation Loss")
        
        axes[0][1].fill_between(X, train_epoch_diceCoefficient_average - train_epoch_diceCoefficient_std,
                                train_epoch_diceCoefficient_average + train_epoch_diceCoefficient_std,
                                color = "r", alpha = 0.1)
        axes[0][1].fill_between(X, validation_epoch_diceCoefficient_average - validation_epoch_diceCoefficient_std,
                                validation_epoch_diceCoefficient_average + validation_epoch_diceCoefficient_std,
                                color = "g", alpha = 0.1)
        
        axes[0][1].set_title("Dice Coefficient Loss")
        axes[0][1].set_xlabel("Epoch")
        axes[0][1].set_ylabel("Loss")
        axes[0][1].legend()
        axes[0][1].grid()





        train_epoch_iou_average = numpy.array([performance.get("iou").get("average") for performance in performance_train_epoch])
        train_epoch_iou_std = numpy.array([performance.get("iou").get("std") for performance in performance_train_epoch])

        validation_epoch_iou_average = numpy.array([performance.get("iou").get("average") for performance in performance_validation_epoch])
        validation_epoch_iou_std = numpy.array([performance.get("iou").get("std") for performance in performance_validation_epoch])


        axes[0][2].plot(X, train_epoch_iou_average, "-o", color = "r", label = "Training Loss")
        axes[0][2].plot(X, validation_epoch_iou_average, "-o", color = "g", label = "Validation Loss")
        
        axes[0][2].fill_between(X, train_epoch_iou_average - train_epoch_iou_std,
                                train_epoch_iou_average + train_epoch_iou_std,
                                color = "r", alpha = 0.1)
        axes[0][2].fill_between(X, validation_epoch_iou_average - validation_epoch_iou_std,
                                validation_epoch_iou_average + validation_epoch_iou_std,
                                color = "g", alpha = 0.1)
        
        axes[0][2].set_title("Intersection over Union Loss")
        axes[0][2].set_xlabel("Epoch")
        axes[0][2].set_ylabel("Loss")
        axes[0][2].legend()
        axes[0][2].grid()
        
        
        
          
        train_epoch_bce_average = numpy.array([performance.get("binary_cross_entropy").get("average") for performance in performance_train_epoch])
        train_epoch_bce_std = numpy.array([performance.get("binary_cross_entropy").get("std") for performance in performance_train_epoch])
        
        validation_epoch_bce_average = numpy.array([performance.get("binary_cross_entropy").get("average") for performance in performance_validation_epoch])
        validation_epoch_bce_std = numpy.array([performance.get("binary_cross_entropy").get("std") for performance in performance_validation_epoch])


        axes[1][0].plot(X, train_epoch_bce_average, "-o", color = "r", label = "Training Loss")
        axes[1][0].plot(X, validation_epoch_bce_average, "-o", color = "g", label = "Validation Loss")
        
        axes[1][0].fill_between(X, train_epoch_bce_average - train_epoch_bce_std,
                                train_epoch_bce_average + train_epoch_bce_std,
                                color = "r", alpha = 0.1)
        axes[1][0].fill_between(X, validation_epoch_bce_average - validation_epoch_bce_std,
                                validation_epoch_bce_average + validation_epoch_bce_std,
                                color = "g", alpha = 0.1)
        
        axes[1][0].set_title("Binary Cross-Entropy Loss")
        axes[1][0].set_xlabel("Epoch")
        axes[1][0].set_ylabel("Loss")
        axes[1][0].legend()
        axes[1][0].grid()




        train_epoch_focal_average = numpy.array([performance.get("focal").get("average") for performance in performance_train_epoch])
        train_epoch_focal_std = numpy.array([performance.get("focal").get("std") for performance in performance_train_epoch])

        validation_epoch_focal_average = numpy.array([performance.get("focal").get("average") for performance in performance_validation_epoch])
        validation_epoch_focal_std = numpy.array([performance.get("focal").get("std") for performance in performance_validation_epoch])

        
        axes[1][1].plot(X, train_epoch_focal_average, "-o", color = "r", label = "Training Loss")
        axes[1][1].plot(X, validation_epoch_focal_average, "-o", color = "g", label = "Validation Loss")
        
        axes[1][1].fill_between(X, train_epoch_focal_average - train_epoch_focal_std,
                                train_epoch_focal_average + train_epoch_focal_std,
                                color = "r", alpha = 0.1)
        axes[1][1].fill_between(X, validation_epoch_focal_average - validation_epoch_focal_std,
                                validation_epoch_focal_average + validation_epoch_focal_std,
                                color = "g", alpha = 0.1)
        
        axes[1][1].set_title("Focal Loss")
        axes[1][1].set_xlabel("Epoch")
        axes[1][1].set_ylabel("Loss")
        axes[1][1].legend()
        axes[1][1].grid()
        
        
                
        matplotlib.pyplot.savefig("analysis_performance_epoch.png")
        #matplotlib.pyplot.savefig(self.resultsDirectory + "/" + "analysis_performance_epoch.png")
        
        
        
        
        
        # Performance by Fold
        ######################################################################
        # self.statistics_train_fold -> [fold+1, fold_performance_avg]
        
       
        fig, axes = matplotlib.pyplot.subplots(2, 3, figsize = (6*3, 7*2))
        
        fig.suptitle("Performance by Fold")
        
        
        X = numpy.arange(1, len(self.statistics_train_fold) + 1, 1)

        performance_train_fold = numpy.array([item[-1] for item in self.statistics_train_fold])
        performance_cross_validation_fold = numpy.array([item[-1] for item in self.statistics_validation_fold])
        

        #train_fold_array = numpy.array([[item[0], item[1], item[2]] for item in self.statistics_train_fold])

        #validation_fold_array = numpy.array([[item[0], item[1], item[2]] for item in self.statistics_validation_fold])

        
        train_fold_accuracy_average = numpy.array([performance.get("accuracy_pixel").get("average") for performance in performance_train_fold])
        train_fold_accuracy_std = numpy.array([performance.get("accuracy_pixel").get("std") for performance in performance_train_fold])
        
        validation_fold_accuracy_average = numpy.array([performance.get("accuracy_pixel").get("average") for performance in performance_cross_validation_fold])
        validation_fold_accuracy_std = numpy.array([performance.get("accuracy_pixel").get("std") for performance in performance_cross_validation_fold])
        
        
        axes[0][0].plot(X, train_fold_accuracy_average, "-o", color = "r", label = "Training Loss")
        axes[0][0].plot(X, validation_fold_accuracy_average, "-o", color = "g", label = "Validation Loss")
        
        axes[0][0].fill_between(X, train_fold_accuracy_average - train_fold_accuracy_std,
                                train_fold_accuracy_average + train_fold_accuracy_std,
                                color = "r", alpha = 0.1)
        axes[0][0].fill_between(X, validation_fold_accuracy_average - validation_fold_accuracy_std,
                                validation_fold_accuracy_average + validation_fold_accuracy_std,
                                color = "g", alpha = 0.1)
        
        axes[0][0].set_title("Accuracy Score")
        axes[0][0].set_xlabel("Fold")
        axes[0][0].set_ylabel("Score")
        axes[0][0].legend()
        axes[0][0].grid()
        
        
        
        
        train_fold_dice_coefficient_average = numpy.array([performance.get("dice_coefficient").get("average") for performance in performance_train_fold])
        train_fold_dice_coefficient_std = numpy.array([performance.get("dice_coefficient").get("std") for performance in performance_train_fold])

        
        validation_fold_dice_coefficient_average = numpy.array([performance.get("dice_coefficient").get("average") for performance in performance_cross_validation_fold])
        validation_fold_dice_coefficient_std = numpy.array([performance.get("dice_coefficient").get("std") for performance in performance_cross_validation_fold])

        axes[0][1].plot(X, train_fold_dice_coefficient_average, "-o", color = "r", label = "Training Loss")
        axes[0][1].plot(X, validation_fold_dice_coefficient_average, "-o", color = "g", label = "Validation Loss")
        
        axes[0][1].fill_between(X, train_fold_dice_coefficient_average - train_fold_dice_coefficient_std,
                                train_fold_dice_coefficient_average + train_fold_dice_coefficient_std,
                                color = "r", alpha = 0.1)
        axes[0][1].fill_between(X, validation_fold_dice_coefficient_average - validation_fold_dice_coefficient_std,
                                validation_fold_dice_coefficient_average + validation_fold_dice_coefficient_std,
                                color = "g", alpha = 0.1)
        
        axes[0][1].set_title("Dice Coefficient Loss")
        axes[0][1].set_xlabel("Fold")
        axes[0][1].set_ylabel("Loss")
        axes[0][1].legend()
        axes[0][1].grid()



        train_fold_iou_average = numpy.array([performance.get("iou").get("average") for performance in performance_train_fold])
        train_fold_iou_std = numpy.array([performance.get("iou").get("std") for performance in performance_train_fold])

        validation_fold_iou_average = numpy.array([performance.get("iou").get("average") for performance in performance_cross_validation_fold])
        validation_fold_iou_std = numpy.array([performance.get("iou").get("std") for performance in performance_cross_validation_fold])

        axes[0][2].plot(X, train_fold_iou_average, "-o", color = "r", label = "Training Loss")
        axes[0][2].plot(X, validation_fold_iou_average, "-o", color = "g", label = "Validation Loss")
        
        axes[0][2].fill_between(X, train_fold_iou_average - train_fold_iou_std,
                                train_fold_iou_average + train_fold_iou_std,
                                color = "r", alpha = 0.1)
        axes[0][2].fill_between(X, validation_fold_iou_average - validation_fold_iou_std,
                                validation_fold_iou_average + validation_fold_iou_std,
                                color = "g", alpha = 0.1)
        
        axes[0][2].set_title("Intersection over Union Loss")
        axes[0][2].set_xlabel("Fold")
        axes[0][2].set_ylabel("Loss")
        axes[0][2].legend()
        axes[0][2].grid()

        
        
        train_fold_bce_average = numpy.array([performance.get("binary_cross_entropy").get("average") for performance in performance_train_fold])
        train_fold_bce_std = numpy.array([performance.get("binary_cross_entropy").get("std") for performance in performance_train_fold])

        
        validation_fold_bce_average = numpy.array([performance.get("binary_cross_entropy").get("average") for performance in performance_cross_validation_fold])
        validation_fold_bce_std = numpy.array([performance.get("binary_cross_entropy").get("std") for performance in performance_cross_validation_fold])

        axes[1][0].plot(X, train_fold_bce_average, "-o", color = "r", label = "Training Loss")
        axes[1][0].plot(X, validation_fold_bce_average, "-o", color = "g", label = "Validation Loss")
        
        axes[1][0].fill_between(X, train_fold_bce_average - train_fold_bce_std,
                                train_fold_bce_average + train_fold_bce_std,
                                color = "r", alpha = 0.1)
        axes[1][0].fill_between(X, validation_fold_bce_average - validation_fold_bce_std,
                                validation_fold_bce_average + validation_fold_bce_std,
                                color = "g", alpha = 0.1)
        
        axes[1][0].set_title("Binary Cross-Entropy over Union Loss")
        axes[1][0].set_xlabel("Fold")
        axes[1][0].set_ylabel("Loss")
        axes[1][0].legend()
        axes[1][0].grid()


        
    
        
        train_fold_focal_average = numpy.array([performance.get("focal").get("average") for performance in performance_train_fold])
        train_fold_focal_std = numpy.array([performance.get("focal").get("std") for performance in performance_train_fold])

        
        validation_fold_focal_average = numpy.array([performance.get("focal").get("average") for performance in performance_cross_validation_fold])
        validation_fold_focal_std = numpy.array([performance.get("focal").get("std") for performance in performance_cross_validation_fold])

        axes[1][1].plot(X, train_fold_focal_average, "-o", color = "r", label = "Training Loss")
        axes[1][1].plot(X, validation_fold_focal_average, "-o", color = "g", label = "Validation Loss")
        
        axes[1][1].fill_between(X, train_fold_focal_average - train_fold_focal_std,
                                train_fold_focal_average + train_fold_focal_std,
                                color = "r", alpha = 0.1)
        axes[1][1].fill_between(X, validation_fold_focal_average - validation_fold_focal_std,
                                validation_fold_focal_average + validation_fold_focal_std,
                                color = "g", alpha = 0.1)
        
        axes[1][1].set_title("Focal Loss")
        axes[1][1].set_xlabel("Fold")
        axes[1][1].set_ylabel("Loss")
        axes[1][1].legend()
        axes[1][1].grid()
        
        
        
        #analysis_performance_fold = os.path.join(self.resultsDirectory, "analysis_performance_fold.png")
        #os.makedirs(analysis_performance_fold)
        
        matplotlib.pyplot.savefig("analysis_performance_fold.png")
        #matplotlib.pyplot.savefig(self.resultsDirectory + "/" + "analysis_performance_fold.png")




        # bar plot de outras métricas
        # if regresion
        #   MSE, RMSE, etc
        # TODO em train obter multiplas metricas 
   



         # Time Analysis
        ######################################################################################

        
        fig, axes = matplotlib.pyplot.subplots(1, 3, figsize = (6*3, 10))
        
        fig.suptitle("Time Analysis")
        
        
        
        
        # Cumulative Training Time Plot     
           
        X = numpy.arange(1, self.n_epochs * self.KFolds + 1, 1)
        
        time_array = numpy.array(self.config.get("train").get("trainingTime_epoch"))

        
        axes[0].plot(X, numpy.cumsum(time_array),
                   '-o', color="b")

        axes[0].set_title("Cumulative Training Time")
        axes[0].set_xlabel("Epochs")
        axes[0].set_ylabel("Time (s)")
        axes[0].legend()
        axes[0].grid()


        matplotlib.pyplot.savefig("analysis_time.png")  
        #matplotlib.pyplot.savefig(self.resultsDirectory + "/" + "analysis_time.png")  

        
        
        # Score by Time Plot
        
        X = numpy.array(self.config.get("train").get("trainingTime_epoch"))



        train_loss_average = numpy.array([performance.get(self.function_loss.loss_type).get("average") for performance in performance_train_epoch])
        train_loss_std = numpy.array([performance.get(self.function_loss.loss_type).get("std") for performance in performance_train_epoch])

        validation_loss_average = numpy.array([performance.get(self.function_loss.loss_type).get("average") for performance in performance_validation_epoch])
        validation_loss_std = numpy.array([performance.get(self.function_loss.loss_type).get("std") for performance in performance_validation_epoch])


        
        #train_epoch_array = numpy.array([[item[0], item[1], item[2]] for item in self.statistics_train_epoch])

        #validation_epoch_array = numpy.array([[item[0], item[1], item[2]] for item in self.statistics_validation_epoch])

        
        
        axes[1].plot(X, train_loss_average,
                   '-o', color="r",label="Training Loss")
        axes[1].plot(X, validation_loss_average,
                   '-o', color="g",label="Validation Loss")
        
        axes[1].fill_between(X, train_loss_average - train_loss_std,
                            train_loss_average + train_loss_std,
                            color="r",alpha = 0.1)
        axes[1].fill_between(X, validation_loss_average - validation_loss_std,
                            validation_loss_average + validation_loss_std,
                            color="g",alpha = 0.1)

        axes[1].set_title("Loss by Time")
        axes[1].set_xlabel("Time (s)")
        axes[1].set_ylabel("Loss")
        axes[1].legend()
        axes[1].grid()
        
        
        
        
        # Fit and Score Time
        ########################
        '''
        fitTime = []
        fitSamples = [100, 200, 500, 1000, 2000, 5000, 10000]
        
        for each fitSample, train model, check for convergance and register train time
        
        '''
        
        
        
        
        
        
        
        
        
        # Model Evaluation Test Data
        ##############################################################################
        
        self.model.eval()
        
        testing_start_time = time.process_time()
        
        values = self.model_test(performance_method = self.performance_method, data_test = None, cv = False)
        testing_loss_avg, testing_loss_std, testing_performance = values
        

        
        print("--> Model Testing Data: Loss (Avg/Std): {0:.2f}/{1:.2f}".format(testing_loss_avg, testing_loss_std))
        print("--> Model Testing Data: Accuracy (Avg/Std): {0:.2f}/{1:.2f}  Dice_Coeff (Avg/Std): {2:.2f}/{3:.2f}  IoU (Avg/Std): {4:.2f}/{5:.2f}".format(
                    testing_performance.get("accuracy_pixel").get("average"), testing_performance.get("accuracy_pixel").get("std"),
                    1 - testing_performance.get("dice_coefficient").get("average"), testing_performance.get("dice_coefficient").get("std"),
                    1 - testing_performance.get("iou").get("average"), testing_performance.get("iou").get("std")))
        
        
        
        
        #make n_predictions and show images, mask and predictions

        if (self.data_type == "image"):

            if (self.problem == "segmentation"):

                fig, axes = matplotlib.pyplot.subplots(n_predictions, 4, figsize = (4*4, 6*n_predictions))

                for i in range(0, n_predictions, 1):

                    index = random.randint(0, self.data_test.__len__()-1)
                    
                    image, mask = self.data_test.__getitem__(index)

                    
                    
                    
                    image_cpu_numpy = image.to("cpu").permute(1, 2, 0).numpy()
                    mask_cpu_numpy = mask.to("cpu").permute(1, 2, 0).numpy()
                    
                    if (self.model_channels_input == 1):
                        axes[i][0].imshow(image_cpu_numpy, cmap = "gray")
                    else:
                        axes[i][0].imshow(image_cpu_numpy)
                    axes[i][0].set_title("Input Test Image: {0}\n Shape {1}".format(index, image_cpu_numpy.shape))

                    axes[i][1].imshow(mask_cpu_numpy, cmap = "gray")
                    axes[i][1].set_title("True Test Mask: {0}\n Shape {1}".format(index, mask_cpu_numpy.shape))

                    
                    image = image.to(self.device)
                    mask = mask.to(self.device)
                    
                    batch_size = 1 # single image
                    image_aux = image.unsqueeze(0) # shape [batch_size, color_channels, width, height]
                    
                    predict = self.model_forward(image_aux)
                    #predict = torch.round(torch.sigmoid(predict))
                    #predict = torch.sigmoid(predict)
                    
                    loss, avg_loss, std_loss = self.function_loss.forward(y_pred = predict, y_true = mask.unsqueeze(0))
                    
                    
                    # assume `predict` is a PyTorch tensor containing an image
                    predict_cpu_numpy = predict.squeeze(0).detach().to("cpu").permute(1, 2, 0).numpy() # convert to numpy array
                            
                    
                    axes[i][2].imshow(predict_cpu_numpy, cmap = "gray")
                    axes[i][2].set_title("Model Prediction: {0}".format(index))

                    
                    predict_sig = torch.sigmoid(predict)
                    predict_sig = predict_sig.squeeze(0).detach().to("cpu").permute(1, 2, 0).numpy() # convert to numpy array
                    
                    axes[i][3].imshow(predict_sig, cmap = "gray")
                    axes[i][3].set_title("Prediction Sigmoid (0.5): {0}\n Loss {1}: {2:.3}".format(index, self.function_loss.loss_type, avg_loss))
        
        
        matplotlib.pyplot.savefig("analysis_predict_test_results.png")  
        #matplotlib.pyplot.savefig(self.resultsDirectory + "/" + "analysis_predict_test_results.png")  
        
 
        
        
        matplotlib.pyplot.show()




    #Data Setup Section
    #########################################################
    def __data_setup(self, data, test_factor, image_size):
        
        self.__data_augmentation(image_size)
        self.__data_generate(data)
        self.__data_preprocessing()
        self.__data_train_test_split(test_factor)
        
    
    def __data_generate(self, data):
        
        if (self.debugMode == True):
            print("-> Data Generation: Initialization")

        start_time = time.process_time()

        if os.path.isdir(data):
             
            self.data_directory = data

            if(self.debugMode == True):
                print("--> {0} is a Directory".format(data))
            

            if (self.data_type == "tabular"):
                pass
        
            elif (self.data_type == "image" and self.problem_type == "supervised"):
                
                # Must have image data and class (Y) data
                
                if (self.problem == "classification"):
                    pass
                elif (self.problem == "detection"):
                    pass
                elif (self.problem == "segmentation"):
                    
                    image_directory = data + "/Images"
                    mask_directory = data + "/Masks"

                    if (os.path.isdir(image_directory) == True):
                        if(self.debugMode == True):
                            print("--> {0} is Image Directory".format(image_directory))
                    else:
                        if(self.debugMode == True):
                            print("--> {0} is Not Image Directory".format(image_directory))
                        raise

                    if (os.path.isdir(mask_directory) == True):
                        if (self.debugMode == True):
                            print("--> {0} is Mask Directory".format(mask_directory))
                    else:
                        if(self.debugMode == True):
                            print("--> {0} is Not Mask Directory".format(mask_directory))
                        raise

                    
                    # Create a dataset using the ImageFolder class
                    #self.data =  torchvision.datasets.ImageFolder(data, transform=self.transformation)
                    self.data = Dataset(image_directory = data + "/Images", mask_directory = data + "/Masks", transform = self.transformation)
    
        else:

            if (self.debugMode == True):
                print("--> {0} is not Directory".format(data))

        
        

        end_time = time.process_time()

        data_loading_time = end_time - start_time
            
        print("--> Data Generation Time: {0}h {1}m {2:.2f}s".format(int(data_loading_time/(60*60)), (data_loading_time % 3600) // 60, data_loading_time%60))

        self.__updateConfig(loadingTime = "{0}h {1}m {2:.2f}s".format(int(data_loading_time/(60*60)), (data_loading_time % 3600) // 60, data_loading_time%60))

        if (self.debugMode == True):
            print("-> Data Generation: Complete")


    def __data_augmentation(self, image_size):




        class transformation_dict_to_PIL_tuple(object):
            """
            Rescale the image in a sample to a given size.

            Args:
                output_size (tuple or int): Desired output size. If tuple, output is
                matched to output_size. If int, smaller of image edges is matched
                to output_size keeping aspect ratio the same.
            """

            def __init__(self):
                pass

            def __call__(self, sample):

                if (isinstance(sample, dict)):
                    image, mask = sample['image'], sample['mask']
                    sample = (image, mask)
                    print("Dict")
                    print(isinstance(sample, dict))
                    print(isinstance(sample, tuple))
                elif (isinstance(sample, tuple)):
                    print("Tuple")
                    print(isinstance(sample, dict))
                    print(isinstance(sample, tuple))

                return sample




        class Resize(object):
            """
            Rescale the image in a sample to a given size.

            Args:
                output_size (tuple or int): Desired output size. If tuple, output is
                    matched to output_size. If int, smaller of image edges is matched
                    to output_size keeping aspect ratio the same.
            """

            def __init__(self, problem, problem_type, output_size):

                assert isinstance(output_size, (int, tuple))
                self.output_size = output_size

                self.problem = problem
                self.problem_type = problem_type

            def __call__(self, sample):


                if (self.problem_type == "supervised"):

                        #Sample X and Y

                        if (self.problem == "segmentation"):

                            image, mask = sample


                            #print("Init: Image Shape {}".format(image.size))
                            #print("Init: Mask Shape {}".format(mask.size))

                            #Format for PIL.Image.Image variable
                            #h, w = image.shape[:2]
                            #w, h = image.size

                            h, w, c = image.shape

                            if isinstance(self.output_size, int):

                                if h > w:
                                    new_h, new_w = self.output_size * h / w, self.output_size
                                else:
                                    new_h, new_w = self.output_size, self.output_size * w / h

                            else:
                                new_h, new_w = self.output_size

                            new_h, new_w = int(new_h), int(new_w)


                            # Convert numpy arrays to PyTorch Tensors
                            image_tensor = torchvision.transforms.ToTensor()(image)
                            mask_tensor = torchvision.transforms.ToTensor()(mask)

                            #image = torchvision.transforms.functional.resize(image, (new_h, new_w))
                            #mask = torchvision.transforms.functional.resize(mask, (new_h, new_w))

                            resize_operation = torchvision.transforms.Resize(self.output_size)

                            resized_image_tensor = resize_operation(image_tensor)
                            resized_mask_tensor = resize_operation(mask_tensor)


                            '''
                            #from skimage import transform
                            #TODO new way
                            #img = transform.resize(image, (new_h, new_w))

                            #img = PIL.Image.fromarray(image)
                            img = image.resize((new_w, new_h))

                            # h and w are swapped for landmarks because for images,
                            # x and y axes are axis 1 and 0 respectively

                            # Convert PIL mask to numpy array
                            mask = numpy.array(mask)
                            mask = mask * [new_w / w, new_h / h]
                            mask = PIL.Image.fromarray(mask)
                            '''


                            #print("Resize: Image Shape {}".format(img.size))
                            #print("Resize: Mask Shape {}".format(mask.size))

                            return (resized_image_tensor, resized_mask_tensor)


        class ToGrayscale(object):
            def __init__(self, active = True):
                
                self.active = active

            def __call__(self, sample):

                if(self.active == False):
                    return sample
                
                image, mask = sample

                grayscale_transform = torchvision.transforms.Grayscale()


                grayscale_image = grayscale_transform(image)


                return (grayscale_image, mask)

        class RandomCrop(object):
            """
            Crop randomly the image in a sample.
            Args:
                output_size (tuple or int): Desired output size. If int, square crop
                is made.
            """

            def __init__(self, problem, problem_type, output_size, active = True):

                self.active = active

                assert isinstance(output_size, (int, tuple))

                if isinstance(output_size, int):
                    self.output_size = (output_size, output_size)
                else:
                    assert len(output_size) == 2
                    self.output_size = output_size
                
                self.problem = problem
                self.problem_type = problem_type


            def __call__(self, sample):
                
                if(self.active == False):
                    return sample

                if (self.problem_type == "supervised"):

                        #Sample X and Y

                        if (self.problem == "segmentation"):                     

                            '''
                            image, mask = sample
                            #Format for PIL.Image.Image variable
                            #h, w = image.shape[:2]
                            w, h = image.size
                            new_h, new_w = self.output_size

                            top = numpy.random.randint(0, h - new_h)
                            left = numpy.random.randint(0, w - new_w)

                            # center crop
                            #top = (new_h - self.output_size) // 2
                            #left = (new_w - self.output_size) // 2
                            if (isinstance(self.output_size, int) == True):
                                image = image.crop((left, top, left + self.output_size, top + self.output_size))
                                mask = mask.crop((left, top, left + self.output_size, top + self.output_size))
                            elif (isinstance(self.output_size, tuple) == True):
                                image = image.crop((left, top, left + self.output_size[0], top + self.output_size[1]))
                                mask = mask.crop((left, top, left + self.output_size[0], top + self.output_size[1]))

                            '''
                            '''
                            image = image[top: top + new_h,
                                        left: left + new_w]

                            mask = mask - [left, top]
                            '''
                            '''
                            '''



                            image, mask = sample

                            h, w = image.size()[-2:]

                            new_h, new_w = self.output_size

                            if h == new_h and w == new_w:
                                return image, mask

                            top = torch.randint(0, h - new_h + 1, size=(1,))
                            left = torch.randint(0, w - new_w + 1, size=(1,))

                            image = image[..., top: top + new_h, left: left + new_w]
                            mask = mask[..., top: top + new_h, left: left + new_w]
                            
                            #image = operation(image)
                            #mask = operation (mask)

                            #print("RandomCrop: Image Shape {}".format(image.size))
                            #print("RadomCrop: Mask Shape {}".format(mask.size))
                            return (image, mask)
                else:
                    print("RandomCrop WTF")


        class CenterCrop(object):
            """
            Crop the image in the center.

            Args:
                output_size (tuple or int): Desired output size. If int, square crop
                    is made.

            """

            def __init__(self, output_size, active = True):

                self.active = active

                assert isinstance(output_size, (int, tuple))
                if isinstance(output_size, int):
                    self.output_size = (output_size, output_size)
                else:
                    assert len(output_size) == 2
                    self.output_size = output_size

            def __call__(self, sample):


                if (self.active == False):
                    return sample

                image, mask = sample

                h, w = image.size()[-2:]
                new_h, new_w = self.output_size

                if h == new_h and w == new_w:
                    return image, mask

                top = (h - new_h) // 2
                left = (w - new_w) // 2

                image = image[..., top: top + new_h, left: left + new_w]
                mask = mask[..., top: top + new_h, left: left + new_w]

                return (image, mask)



        class RandomScale(object):

            def __init__(self, scale_range=(0.9, 1.2), active = True):
 
                self.scale_range = scale_range
                self.active = active
    
            def __call__(self, sample):

                if self.active == False:
                    return sample
                
                image, mask = sample
                scale_factor = random.uniform(self.scale_range[0], self.scale_range[1])

                # calculate the new size of the image and mask
                transform = torchvision.transforms.ToPILImage()

                image =  transform(image)
                mask =  transform(mask)

                new_size = [int(scale_factor * s) for s in image.size]

                image = torchvision.transforms.functional.resize(image, new_size)
                mask = torchvision.transforms.functional.resize(mask, new_size)


                image = torchvision.transforms.functional.pil_to_tensor(image)
                mask = torchvision.transforms.functional.pil_to_tensor(mask)

                return (image, mask)




        class RandomRotation(object):
            """
            Randomly rotate the given PyTorch tensors `img1` and `img2` by `degrees` with a probability `p`.
            The two images will be rotated by the same angle.
            Args:
                img1 (Tensor): PyTorch tensor image to be rotated.
                img2 (Tensor): PyTorch tensor image to be rotated.
                degrees (float or sequence): Range of degrees to select from.
                    If degrees is a number instead of sequence like (min, max), the range of degrees
                    will be (-degrees, +degrees).
                p (float): probability of rotation.
                fill (tuple): RGB color tuple for filling the area outside the rotated image.
            Returns:
                tuple(Tensor, Tensor): Two rotated PyTorch tensor images.
            """

            def __init__(self, degrees, active = True, probability = 0.5, fill=(0,)):
                
                self.active = active
                self.degrees = degrees
                self.probability = probability
                self.fill = fill

            def __call__(self, sample):

                if (self.active == False):
                    return sample

                '''
                if isinstance(sample, tuple):
                    image, mask = sample
                else:
                    print("Not Tuple")
                    raise

                angle = random.uniform(-self.degrees, self.degrees)

                #img = PIL.Image.fromarray(image)
                image = image.rotate(angle)

                mask = numpy.asarray(mask)
                #new_mask.reshape(224, 224, 1)
                mask = mask.reshape(-1, 2)

                # rotate mask
                angle = angle * 3.14159 / 180
                c, s = numpy.cos(angle), numpy.sin(angle)
                rotation_matrix = numpy.array([[c, -s], [s, c]])
                new_mask = numpy.dot(mask, rotation_matrix)

                mask = PIL.Image.fromarray(new_mask.reshape(224, 224))

                print(type(image))
                print(type(mask))
                '''


                '''
                image, mask = sample

                            
                operation = torchvision.transforms.RandomRotation(degrees = self.degrees, 
                                                                interpolation=torchvision.transforms.InterpolationMode.NEAREST,
                                                                expand=False, center=None, fill=0)
                image = operation(image)
                mask = operation(mask)
                '''
                
                image, mask = sample

                if (random.random() < self.probability):

                    angle = random.uniform(-self.degrees, self.degrees)
                    image = torchvision.transforms.functional.rotate(image, angle, fill = self.fill)
                    mask = torchvision.transforms.functional.rotate(mask, angle, fill = self.fill)


                #print("RandomRotation: Image Shape {}".format(image.size))
                #print("RandomRotation: Mask Shape {}".format(mask.size))

                return (image, mask)


        class HorizontalFlip(object):
            """
            Flip the image and landmarks horizontally.
            """

            def __init__(self, problem, problem_type, probability = 0.5, active = True):

                self.problem = problem
                self.problem_type = problem_type
                self.probability = probability
                self.active = active

            def __call__(self, sample):

                if(self.active == False):
                    return sample

                if (self.problem_type == "supervised"):

                        #Sample X and Y

                        if (self.problem == "segmentation"):

                            '''
                            image, mask = sample
                            
                            image = numpy.fliplr(image)
                            #mask = numpy.asarray(mask)

                            image = PIL.Image.fromarray(image)
                            #h, w = image.shape[:2]
                            w, h = image.size



                            mask = numpy.array(mask) # convert to numpy array
                            
                            #mask = w - mask # invert mask horizontally

                            #mask[:, 0] = w - mask[:, 0]
                            mask = mask[:, ::-1]
                            #mask[:, 0] = image.shape[1] - mask[:, 0]

                            mask = PIL.Image.fromarray(mask.reshape(224, 224))
                            #print("HorizontalFlip: Image Shape {}".format(image.size))
                            #print("HorizontalFlip: Mask Shape {}".format(mask.size))


                            '''


                            image, mask = sample

                            if (random.random() < self.probability):
                                image = torch.flip(image, dims=[2])
                                mask = torch.flip(mask, dims=[2])




                            return (image, mask)


        class VerticalFlip(object):
            """
            Flip the image and landmarks vertically.
            """
            def __init__(self, problem, problem_type, probability = 0.5, active = True):

                self.problem = problem
                self.problem_type = problem_type
                self.probability = probability
                self.active = active

            def __call__(self, sample):

                if(self.active == False):
                    return sample

                if (self.problem_type == "supervised"):

                        #Sample X and Y

                        if (self.problem == "segmentation"):

                            '''
                            image, mask = sample

                            image = numpy.flipud(image)
                            #mask[:, 1] = image.shape[0] - mask[:, 1]



                            
                            print(image.shape)
                            image = PIL.Image.fromarray(image)
                            #h, w = image.shape[:2]
                            w, h = image.size



                            mask = numpy.array(mask) # convert to numpy array
                            #mask = h - mask # invert mask horizontally
                            print(mask.shape)
                            mask[:, 1] = h - mask[:, 1]


                            #mask[:, 0] = w - mask[:, 0]
                            #mask[:, 0] = image.shape[1] - mask[:, 0]

                            mask = PIL.Image.fromarray(mask.reshape(224, 224))
                            print("VerticalFlip: Image Shape {}".format(image.size))
                            print("VerticalFlip: Mask Shape {}".format(mask.size))


                            '''
                            '''
                            image, mask = sample

                            
                            operation = torchvision.transforms.RandomVerticalFlip(p=self.probability)
                            image = operation(image)
                            mask = operation(mask)
                            '''
                            image, mask = sample

                            if (random.random() < self.probability):
                                image = torch.flip(image, dims=[1])
                                mask = torch.flip(mask, dims=[1])


                            return (image, mask)


        class RandomBrightnessContrast:

            def __init__(self, brightness_range=(-0.2, 0.2), contrast_range=(-0.2, 0.2), p = 0.5, active = True):

                self.brightness_range = brightness_range
                self.contrast_range = contrast_range
                self.probability = p
                self.active = active

            def __call__(self, sample):

                if (self.active == False):
                    return sample
                
                if numpy.random.rand() > self.probability:
                    return sample

                image, mask = sample

                transform = torchvision.transforms.ToPILImage()

                #image =  transform(image)

                contrast_factor = 1.0 + random.uniform(self.contrast_range[0], self.contrast_range[1])
                brightness_factor = 1.0 + random.uniform(self.brightness_range[0], self.brightness_range[1])
    

                image = torchvision.transforms.functional.adjust_brightness(image, brightness_factor)
                image = torchvision.transforms.functional.adjust_contrast(image, contrast_factor)


                #image = torchvision.transforms.functional.pil_to_tensor(image)
                
                return (image, mask)


        class Normalize():

            def __init__(self, active = True, img_normalization = True):

                self.active = active
                self.img_normalization = img_normalization

            def __call__(self, sample):

                if (self.active == False):
                    return sample
                
                image, mask = sample

                
                # Image Pre-Processing
                #############################################################



                # Image Histogram Equalization

                #image_int = image.to(torch.uint8)

                '''
                # Convert the tensor to a NumPy array
                image_np = image.permute(1, 2, 0).cpu().numpy() 
  
                # Convert the NumPy array to an OpenCV object
                image_cv = cv2.cvtColor(image_np, cv2.COLOR_RGB2YCrCb)



                # Convert the numpy array to an OpenCV object
                if image_cv.dtype == numpy.float32:
                    image_cv = (image_cv * 256).astype(numpy.uint8)
                else:
                    image_cv = image_cv.astype(numpy.uint8)


                # create a CLAHE object (Arguments are optional).
                clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))

                y_channel = image_cv[:,:,0]
                y_channel_clahe = clahe.apply(y_channel)

                # Replace Y channel with CLAHE applied Y channel
                img_clahe = image_cv.copy()
                img_clahe[:,:,0] = y_channel_clahe

                img_rgb = numpy.asarray(cv2.cvtColor(img_clahe, cv2.COLOR_YCrCb2RGB))

                # Convert the NumPy array to a PyTorch tensor
                #tensor = torch.from_numpy(img_rgb.transpose(2,0,1))
                #image_np = np.expand_dims(image_cv, axis=-1)  # add single channel dimension
                tensor = torch.from_numpy(img_rgb.transpose((2, 0, 1)))  # convert to PyTorch tensor

                image = tensor.float()
                '''
                '''
                # Convert the tensor to a NumPy array
                image_np = image.permute(1, 2, 0).cpu().numpy() 
                
                # Convert the NumPy array to an OpenCV object
                if image_np.dtype == numpy.float32:
                    image_np = (image_np * 256).astype(numpy.uint8)
                else:
                    image_np = image_np.astype(numpy.uint8)

                # create a CLAHE object (Arguments are optional).
                clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))

                # Convert the image to grayscale
                gray = cv2.cvtColor(image_np, cv2.COLOR_RGB2GRAY)

                # Apply CLAHE to the grayscale image
                gray_clahe = clahe.apply(gray)

                # Convert the grayscale image back to RGB
                img_clahe = cv2.cvtColor(gray_clahe, cv2.COLOR_GRAY2RGB)

                # Convert the NumPy array to a PyTorch tensor
                tensor = torch.from_numpy(img_clahe.transpose(2,0,1))

                image = tensor.float()
                '''

                '''
                # Convert the tensor to a NumPy array
                image_np = image.permute(1, 2, 0).cpu().numpy()

                # Convert the NumPy array to an OpenCV object
                if image_np.dtype == numpy.float32:
                    image_cv = (image_np * 256).astype(numpy.uint8)
                else:
                    image_cv = image_np.astype(numpy.uint8)

                # Convert the image to YCrCb color space
                img_HSV = cv2.cvtColor(image_cv, cv2.COLOR_RGB2HSV)

                # create a CLAHE object (Arguments are optional).
                clahe = cv2.createCLAHE(clipLimit=100.0, tileGridSize=(10,10))

                # Apply CLAHE to the Y channel of the YCrCb image
                img_HSV[:,:,0] = clahe.apply(img_HSV[:,:,0])

                # Convert the image back to RGB
                img_clahe = cv2.cvtColor(img_HSV, cv2.COLOR_HSV2BGR)

                # Convert the NumPy array to a PyTorch tensor
                tensor = torch.from_numpy(img_clahe.transpose(2,0,1))

                image = tensor.float()
                

                #image = image.to(torch.uint8)
                #image = torchvision.transforms.functional.equalize(image)
                '''
                
                # Image Normalization
                
                if (self.img_normalization == True and torch.max(image) > 1):

                    # Compute the mean and standard deviation of the pixel values in the input image for each color channel separately
                    mean = torch.mean(image, dim=(1, 2))
                    std = torch.std(image, dim=(1, 2))

                    # Define a normalization transform using the computed mean and standard deviation
                    normalize = torchvision.transforms.Normalize(mean=mean.tolist(), std=std.tolist())

                    image = normalize(image)

                

                

                

                mask = mask.float()
                # Mask Pre-Processing
                ##############################################################



                # Mask Normalization
                if (torch.max(mask) > 1):

                    mask = mask / 255.0 #Max value for 8 bit image

                    out_of_range = torch.logical_or(mask < 0, mask > 1)
                    in_of_range = torch.logical_or(mask > 0, mask < 1)

                    mask.to(torch.uint8)

                    if(torch.any(out_of_range) or torch.any(in_of_range)):
                        print("X ERROR: Mask Normalization -> Values Besides 0 and 1")
                







                return (image, mask)
            

        class ToTensor(object):
            """Convert ndarrays in sample to Tensors."""

            def __init__(self, problem, problem_type):

                self.problem = problem
                self.problem_type = problem_type

            def __call__(self, sample):

                if (self.problem_type == "supervised"):

                    #Sample X and Y

                    if (self.problem == "segmentation"):

                        image, mask = sample
                        if (isinstance(image, torch.Tensor) and isinstance(mask, torch.Tensor)):

                            image = image.to(torch.float32)
                            mask = mask.to(torch.float32)

                            if(torch.isnan(image).any()):
                                print("Image has NaN values")
                                matplotlib.pyplot.imshow(numpy.transpose(image.numpy(), (1, 2, 0)))
                                matplotlib.pyplot.show()

                            elif(torch.isnan(mask).any()):
                                print("Mask has NaN values")
                                matplotlib.pyplot.imshow(numpy.transpose(mask.numpy(), (1, 2, 0)))
                                matplotlib.pyplot.show()

                            sample = (image, mask)

                            return sample

                        #print("ToTensor: Image Shape {}".format(image.size))
                        #print("ToTensor: Mask Shape {}".format(mask.size))
                        # swap color axis because
                        # numpy image: H x W x C
                        # torch image: C x H x W
                        #image = image.transpose((2, 0, 1))

                        #image = image.transpose((0,1))
                        #image = torch.from_numpy(image)
                        #mask = torch.from_numpy(mask)

                        #print("Final: Image Shape {}".format(image.size))
                        #print("Final: Mask Shape {}".format(mask.size))

                        transform = torchvision.transforms.ToTensor()
                        image = transform(image)
                        mask = transform(mask)

                        image = image.to(torch.float32)
                        mask = mask.to(torch.float32)

                        if(torch.isnan(image).any()):
                            print("Image has NaN values")
                            matplotlib.pyplot.imshow(numpy.transpose(image.numpy(), (1, 2, 0)))
                            matplotlib.pyplot.show()

                        elif(torch.isnan(mask).any()):
                            print("Mask has NaN values")
                            matplotlib.pyplot.imshow(numpy.transpose(mask.numpy(), (1, 2, 0)))
                            matplotlib.pyplot.show()

                        #image = image.transpose((2, 0, 1))
                        
                        #print("Final: Image  {}".format(type(image.size)))
                        #print("Final: Mask  {}".format(type(mask.size)))

                        return (image, mask)



        Resize_active = True

        ToGrayscale_active = self.model_channels_input == 1
        HorizontalFlip_active= True
        HorizontalFlip_probability = 0.5

        VerticalFlip_active = True
        VerticalFlip_probability = 0.5

        RandomCrop_active = False

        RandomRotation_active = False
        RandomRotation_degrees = 30.0
        RandomRotation_probability = 0.7

        CenterCrop_active = False
        
        RandomScale_active = False

        RandomBrightnessContrast_active = True
        RandomBrightnessContrast_brightness_range = (-0.1, 0.1)
        RandomBrightnessContrast_contrast_range = (-0.2, 0.2)
        RandomBrightnessContrast_probability = 0.8

        Normalize_active = True
        Normalize_img_normalization = True


        self.transformation = torchvision.transforms.Compose([Resize(problem = self.problem, problem_type = self.problem_type, output_size = image_size),
                                                            
                                                            ToGrayscale(active = ToGrayscale_active),
                                                            HorizontalFlip(active = HorizontalFlip_active, problem = self.problem, problem_type = self.problem_type, probability = HorizontalFlip_probability),
                                                            VerticalFlip(active = VerticalFlip_active, problem = self.problem, problem_type = self.problem_type, probability = VerticalFlip_probability),
                                                            
                                                            RandomCrop(active = RandomCrop_active, problem = self.problem, problem_type = self.problem_type, output_size = image_size),
                                                            RandomRotation(active = RandomRotation_active, degrees = RandomRotation_degrees, probability = RandomRotation_probability),
                                                            CenterCrop(active = CenterCrop_active, output_size = image_size),
                                                            RandomScale(active = RandomScale_active),
                                                            
                                                            RandomBrightnessContrast(active = RandomBrightnessContrast_active, brightness_range=RandomBrightnessContrast_brightness_range, contrast_range=RandomBrightnessContrast_contrast_range, p = RandomBrightnessContrast_probability),
                                                            
                                                            Normalize(active = Normalize_active, img_normalization = Normalize_img_normalization),
                                                            ToTensor(problem = self.problem, problem_type = self.problem_type)
                                                            ])


        transformation_list = [
                                "Resize({0}) -> active:{1}".format(image_size, Resize_active), 
                                "ToGrayscale() -> active:{0}".format(ToGrayscale_active), 
                                "HorizontalFlip(probability = {0}) -> active:{1}".format(HorizontalFlip_probability, HorizontalFlip_active),
                                "VerticalFlip(probability = {0}) -> active:{1}".format(VerticalFlip_probability, VerticalFlip_active),
                                
                                "RandomCrop() -> active: {0}".format(RandomCrop_active),
                                "RandomRotation(degrees = {0}, probability = {1}) -> active:{2}".format(RandomRotation_degrees, RandomRotation_probability, RandomRotation_active),
                                "CenterCrop() -> active {0}".format(CenterCrop_active),
                                "RandomScale() -> active: {0}".format(RandomScale_active),
                                
                                "RandomBrightnessContrast(birghtness_range = {0}, contrast_range = {1}, probability = {2}) -> active:{3}".format(RandomBrightnessContrast_brightness_range, RandomBrightnessContrast_brightness_range, RandomBrightnessContrast_probability, RandomBrightnessContrast_active),

                                "Normalize(img_normalization {0}) -> active:{1}".format(Normalize_img_normalization, Normalize_active),
                                "ToTensor()"
                            ]


        self.__updateConfig(transformations=transformation_list)

        '''
        torchvision.transforms.Normalize(
                                                                mean=[0.485, 0.456, 0.406],
                                                                std=[0.229, 0.224, 0.225]
                                                            )
        Output different form input, so normalize diferent shapes para Image e para masks. Nao sei como separar
        '''


    def __data_preprocessing(self):
        pass
       
    
    def __data_train_test_split(self, test_factor):

        if (self.debugMode == True):
            print("-> Train/Test Split: Initialization")

        # Split your dataset into training and testing subsets
        test_size = int(test_factor * self.data.__len__())
        train_size = self.data.__len__() - test_size


        self.__updateConfig(train_size = train_size, test_size = test_size)

        def random_sublist(lst, num):
            sublist = random.sample(lst, num)
            remaining = [elem for elem in lst if elem not in sublist]
            return sublist, remaining
        
        train_list, test_list = random_sublist(self.data.data_directorys, train_size)

        self.data_train = Dataset(data_directorys = train_list, transform = self.transformation)
        self.data_test = Dataset(data_directorys = test_list, transform = self.transformation)
        
        
        if (self.debugMode == True):
            print("--> Train/Test Split: Test Factor = {0} Train Size = {1} Test Size = {2}".format(
                test_factor, self.data_train.__len__(), self.data_test.__len__()))

    
        if (self.debugMode == True):
            print("-> Train/Test Split: Complete")


    def __get_custom_sample(self, data, index, original = False):

        if (self.setup_mark != True):
            raise
            return




        image, mask = data.data_directorys[index]

        image = cv2.imread(image)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        mask = cv2.imread(mask, cv2.IMREAD_GRAYSCALE)

        image = PIL.Image.fromarray(numpy.uint8(image)).convert('RGB')
        mask = PIL.Image.fromarray(numpy.uint8(mask)).convert('L')
            
        sample = (image, mask)

   
        # Apply transformations
        if (data.transform != None and original == False):
            
            sample = data.transform(sample)
        
        return sample






    #Model Setup Section
    #########################################################
    def __model_setup(self, model):
        self.__model_build(model = model)
        self.__parameter_setup(model = model)


    def __model_build(self, model):

        if (self.debugMode == True):
            print("-> Model Build: Initialization")

        if (isinstance(model, torch.nn.Module)):

            self.model = model
            self.transferLearning = True
        
        elif (isinstance(model, str)):

            if (self.model_name == "unet"):

                self.__model_build_UNet()

                if (self.debugMode == True):
                    print("--> Model Build: U-Net Original")

            elif (self.model_name == "unet-batch_norm"):

                self.model = UNET(in_channels=self.model_channels_input, out_channels=self.model_channels_output).to(self.device)
                
                if (self.debugMode == True):
                    print("--> Model Build: U-Net with Batch Normalization")

        #self.model.blocks = torch.tensor(self.model.blocks)
        self.model_built = True
        self.model.to(self.device)
        
        #print("-> Model Device: {}".format(next(self.model.parameters()).device))

        if (self.debugMode == True):
            print("-> Model Build: Complete")
        

    def __model_build_UNet(self):
        
        if (self.debugMode == True):
            print("--> Build U-Net: Initialization")
        
        # Encoder Chain
        # The contracting path follows the typical architecture of a convolutional network.
        # It consists of the repeated application of two 3×3 convolutions
        # (unpadded convolutions), each followed by a rectified linear unit (ReLU)
        # and a 2×2 max pooling operation with stride 2 for downsampling.
        # At each downsampling step we double the number of feature channels.

        self.model.blocks.append(DoubleConvolutionBlock(self.model_channels_input, 64, "Down"))
        self.model.blocks.append(DoubleConvolutionBlock(64, 128, "Down"))
        self.model.blocks.append(DoubleConvolutionBlock(128, 256, "Down"))
        self.model.blocks.append(DoubleConvolutionBlock(256, 512, "Down"))

    
        # Decoder Chain
        # Every step in the expansive path consists of an upsampling of the feature map
        # followed by a 2×2 convolution (“up-convolution”) that halves the number
        # of feature channels, a concatenation with the correspondingly cropped feature map
        # from the contracting path, and two 3×3 convolutions, each followed by a ReLU

        self.model.blocks.append(DoubleConvolutionBlock(512, 1024, "Up"))
        
        self.model.blocks.append(DoubleConvolutionBlock(1024, 512, "Up"))
        self.model.blocks.append(DoubleConvolutionBlock(512, 256, "Up"))
        self.model.blocks.append(DoubleConvolutionBlock(256, 128, "Up"))

        self.model.blocks.append(DoubleConvolutionBlock(128, 64, "Final", self.model_channels_output))


        self.model_name = "unet"
        '''
        for i in range(0, len(self.model.blocks), 1):
            self.model.blocks[i] = torch.nn.Parameter(self.blocks[i])
        '''
        if (self.debugMode == True):
            print("--> Build U-Net: Complete")


    def __parameter_setup(self, model):
        
        if (self.debugMode == True):
            print("-> Model Parameter Setup: Initialization")

        # Ensure Freezing of the Last 20% Parameters
        # In Transfer Learning Fine Tuning Tecnique

        if (self.transferLearning == True):
            freezing_percentage = 0.7
            total_params = sum(p.numel() for p in model.parameters())
            index = int(total_params * freezing_percentage)
            current_params = 0
            params_require = 0
            for name, param in model.named_parameters():
                if current_params < index:
                    param.requires_grad = False
                else:
                    param.requires_grad = True
                    params_require += param.numel()
                current_params += param.numel()

            print("--> Model Has {0:.2f} Learnable Parameters and {1:.2f} Not Learnable Parameters".format(
                params_require/current_params, (current_params-params_require)/current_params))
        if (self.debugMode == True):
            print("-> Model Parameter Setup: Complete")


    def model_forward(self, X):
    
        Y_pred = None

        if (self.data_type == "image"):

            if (self.problem == "segmentation"):

                # model will return a mask
                if (self.transferLearning == True):

                    mask = self.model(X)

                else:
                    mask = self.model.forward(X)

                if (mask.shape[0] >= 1): # batch size > 1 (multiple inputs and multiple outputs)

                    Y_pred = mask
                else:
                    print("X ERROR: Model Forward: Output Shape Error")


        return Y_pred


    def __model_debug(self, image_size):
        

        batch_size = 5

        X = torch.randn((batch_size, self.model_channels_input, image_size, image_size), device=self.device)

        print("--> Debug: Input Device {0} Model Device {1}".format(X.device, next(self.model.parameters()).device))
    
        if (self.model_built == False):
            print("X ERROR -> Debug: Model Not Built Yet")

        pred = self.model_forward(X)
        print("--> Debug: Input Shape {0} Predict Shape {1}".format(X.shape, pred.shape))
        
        condition = (batch_size == pred.shape[0]) and (pred.shape[1] == self.model_channels_output)

        assert condition, AssertionError
        print("--> Debug: Prediction OK")
        

    def __save_model(self):
        torch.save(self.state_dict(), path)
    

    def __load_model(self):
        pass




    #Train Model Section
    ########################################################################################

    def train(self, batch_size = 32, n_epochs = 5, KFolds = 5,
            function_loss = None, optimizer = None, performance_method = "accuracy", 
            multiple_processes = 2, best_batch_show = True):
        

        def plot_grad_flow(named_parameters, axes):
            '''Plots the gradients flowing through different layers in the net during training.
            Can be used for checking for possible gradient vanishing / exploding problems.
            
            Usage: Plug this function in Trainer class after loss.backwards() as 
            "plot_grad_flow(self.model.named_parameters())" to visualize the gradient flow'''

            matplotlib.pyplot.cla() # Clear axis

            ave_grads = []
            max_grads= []
            layers = []

            for n, p in named_parameters:

                if(p.requires_grad) and ("bias" not in n):
                    layers.append(n)
                    ave_grads.append(p.grad.cpu().abs().mean())
                    max_grads.append(p.grad.cpu().abs().max())

            axes.bar(numpy.arange(len(max_grads)), max_grads, alpha=0.3, lw=1, color="c")
            axes.bar(numpy.arange(len(max_grads)), ave_grads, alpha=0.3, lw=1, color="b")
            axes.hlines(0, 0, len(ave_grads)+1, lw=2, color="k" )

            #matplotlib.pyplot.bar(numpy.arange(len(max_grads.to("cpu").numpy())), max_grads.to("cpu").numpy(), alpha=0.1, lw=1, color="c")
            #matplotlib.pyplot.bar(numpy.arange(len(max_grads.to("cpu").numpy())), ave_grads.to("cpu").numpy(), alpha=0.1, lw=1, color="b")
            #matplotlib.pyplot.hlines(0, 0, len(ave_grads.to("cpu").numpy())+1, lw=2, color="k" )

            
            matplotlib.pyplot.xticks(range(0,len(ave_grads), 1), layers, rotation="vertical")
            matplotlib.pyplot.xlim(left=0, right=len(ave_grads))
            matplotlib.pyplot.ylim(bottom = -0.001, top=0.2) # zoom in on the lower gradient regions
            matplotlib.pyplot.xlabel("Layers")
            matplotlib.pyplot.ylabel("Gradient Average")
            matplotlib.pyplot.title("Gradient flow")
            matplotlib.pyplot.grid(True)
            matplotlib.pyplot.legend([matplotlib.lines.Line2D([0], [0], color="c", lw=4),
                        matplotlib.lines.Line2D([0], [0], color="b", lw=4),
                        matplotlib.lines.Line2D([0], [0], color="k", lw=4)], ['max-gradient', 'mean-gradient', 'zero-gradient'])


        self.batch_size = batch_size
        self.n_epochs = n_epochs
        self.KFolds = KFolds

        self.function_loss = Function_Loss(loss_type = function_loss, problem = self.problem, problem_type = self.problem_type, device = self.device)
        self.optimizer = optimizer
        self.performance_method = performance_method
        data_number_total = 0

        trainingTime_epoch = []
        n_epochs_total = 0

        
        print("Model Training: Initialization")


        print("-> Optimizer: Adam")
        print("-> Function Loss: {0}".format(self.function_loss.loss_type))
        print("-> Performance Evaluation Method: {}".format(self.performance_method))

        
        train_start_time = time.process_time()

        # Create a KFold object with the desired number of folds
        kFold = sklearn.model_selection.KFold(n_splits = KFolds, shuffle=True)


        scaler = torch.cuda.amp.GradScaler()

        # Iterate over the folds
        for fold, (train_idx, valid_idx) in enumerate(kFold.split(self.data_train)):

            fitTime = time.process_time()

            


    
            fold_loss_avg = 0.0
            fold_loss_std = 0.0
            fold_performance = {}

            print("-> Fold {0}/{1}".format(fold+1, KFolds))

            # Create the data loaders for the fold using the SubsetRandomSampler
            train_sampler = torch.utils.data.SubsetRandomSampler(train_idx)
            valid_sampler = torch.utils.data.SubsetRandomSampler(valid_idx)
    
            train_loader = torch.utils.data.DataLoader(self.data_train, batch_size=batch_size, sampler=train_sampler)
            valid_loader = torch.utils.data.DataLoader(self.data_train, batch_size=batch_size, sampler=valid_sampler)


            # Train the model using the data from this fold

            for epoch in range (0, n_epochs, 1):
                

                self.worstSample = [None, None, None]
                self.worstSample_loss = 0.0
                self.bestSample = [None, None, None]
                self.bestSample_loss = 1.0
                self.randomSample = [None, None, None]
                self.randomSample_loss = 0.0

                self.model.train()
 
                time_epoch, epoch_loss_avg, epoch_loss_std, epoch_performance = self.__train_epoch(kFold = fold, epoch = epoch, 
                                                                                            performance_method = self.performance_method,
                                                                                            data = train_loader, scaler = scaler, 
                                                                                            data_number_total = data_number_total)
                

                n_epochs_total += 1
                self.statistics_train_epoch.append([n_epochs_total, epoch_performance])


                fold_loss_avg += epoch_loss_avg
                fold_loss_std += epoch_loss_std
                self.__sum_dictionaries(fold_performance, epoch_performance)


                
                trainingTime_epoch.append(time_epoch)




                # Non Traditional Validation by Epoch aswell
                ##################################################################

                print("--> Model Training: Cross-Validation by Epoch")

                values = self.model_test(performance_method = self.performance_method, data_test = valid_loader, cv = True)

                validation_loss_avg, validation_loss_std, validation_performance = values

                self.statistics_validation_epoch.append([epoch+1, validation_loss_avg, validation_loss_std, validation_performance])

                

                ###################################################################

                '''
                print("--> Loss (Avg/Std):  {0:.2f}/{1:.2f} Acc Pixel(Avg/Std): {2:.2f}/{3:.2f} Dice Coeff (Avg/Std): {4:.2f}/{5:.2f}".format(
                    validation_loss_avg, validation_loss_std, 
                    validation_performance.get("accuracy_pixel").get("average"), validation_performance.get("accuracy_pixel").get("std"),
                    validation_performance.get("dice_coefficient").get("average"), validation_performance.get("dice_coefficient").get("std")
                    ))
                
                '''


                print("--> Epoch {0} of {1} | Time: {2}h{3}m{4:.2f}s Train_Loss (Avg/Std): {5:.2f}/{6:.2f} Validation_Loss (Avg/Std):{7:.2f}/{8:.2f}".format(
                        epoch + 1, n_epochs, int(time_epoch/(60*60)), (time_epoch % 3600) // 60, time_epoch%60,
                        epoch_loss_avg, epoch_loss_std,
                        validation_loss_avg, validation_loss_std))
            


                for name, param in self.model.named_parameters():
                    if torch.isnan(param).any():
                        print("Model Parameter {0} has NaN values".format(name))
                        print("Parameter Values: {0}".format(param))

                



                fig, axes = matplotlib.pyplot.subplots(1,1, figsize = (8,4))
                plot_grad_flow(self.model.named_parameters(),axes)




                fig, axes = matplotlib.pyplot.subplots(3,3, figsize = (16,14))

                fig.suptitle("Highest Loss: {0:.3f} Random Loss: {1:.3f} Lowest Loss: {2:.3f}".format(
                    self.worstSample_loss, self.randomSample_loss, self.bestSample_loss), fontsize = 16)

                if (self.model_channels_input == 1):
                    axes[0][0].imshow(self.worstSample[0].permute(1, 2, 0).cpu().detach().numpy(), cmap = "gray")
                else:
                    axes[0][0].imshow(self.worstSample[0].permute(1, 2, 0).cpu().detach().numpy())
                axes[0][0].set_title("Original Image")

                axes[0][1].imshow(self.worstSample[1].permute(1, 2, 0).cpu().detach().numpy(), cmap = "gray")
                axes[0][1].set_title("Ground Truth Mask Image")

                axes[0][2].imshow(self.worstSample[2].permute(1, 2, 0).cpu().detach().numpy(), cmap = "gray")
                axes[0][2].set_title("Model Prediction + Sigmoid")

                if (self.model_channels_input == 1):
                    axes[1][0].imshow(self.randomSample[0].permute(1, 2, 0).cpu().detach().numpy(), cmap = "gray")
                else:
                    axes[1][0].imshow(self.randomSample[0].permute(1, 2, 0).cpu().detach().numpy())
                axes[1][0].set_title("Original Image")

                axes[1][1].imshow(self.randomSample[1].permute(1, 2, 0).cpu().detach().numpy(), cmap = "gray")
                axes[1][1].set_title("Ground Truth Mask Image")

                axes[1][2].imshow(self.randomSample[2].permute(1, 2, 0).cpu().detach().numpy(), cmap = "gray")
                axes[1][2].set_title("Model Prediction + Sigmoid")

                if (self.model_channels_input == 1):
                    axes[2][0].imshow(self.bestSample[0].permute(1, 2, 0).cpu().detach().numpy(), cmap = "gray")
                else:
                    axes[2][0].imshow(self.bestSample[0].permute(1, 2, 0).cpu().detach().numpy())
                axes[2][0].set_title("Original Image")

                axes[2][1].imshow(self.bestSample[1].permute(1, 2, 0).cpu().detach().numpy(), cmap = "gray")
                axes[2][1].set_title("Ground Truth Mask Image")

                axes[2][2].imshow(self.bestSample[2].permute(1, 2, 0).cpu().detach().numpy(), cmap = "gray")
                axes[2][2].set_title("Model Prediction + Sigmoid")
                
                matplotlib.pyplot.show()


            # Adjust metrics to get average loss and accuracy per batch 
            fold_loss_avg = fold_loss_avg / n_epochs
            fold_loss_std = fold_loss_std / n_epochs
            self.__division_dict_float(fold_performance, n_epochs)
            fold_performance_avg = fold_performance


            self.statistics_train_fold.append([fold+1, fold_performance_avg])






            # Validation Section by Fold
            #######################################################################################

            #print("--> Model Training: Cross-Validation by Fold")

            #values = self.model_test(performance_method = self.performance_method, data_test = valid_loader, cv = True)

            #validation_loss_avg, validation_loss_std, validation_performance = values
            values = self.statistics_validation_epoch[-1]
            validation_loss_avg = values[1]
            validation_loss_std = values[2]
            validation_performance = values[3]
            #print(validation_performance)
            self.statistics_validation_fold.append([fold+1, validation_loss_avg, validation_loss_std, validation_performance])

            #######################################################################################




            if (self.performance_method == "accuracy_pixel"):

                print("-> Fold {0} Train_Loss (Avg/Std): {1:.2f}/{2:.2f} Train Acc (Avg/Std): {3:.2f}/ Validation Loss (Avg/Std): {4:.2f}/{5:.2f} Validation Acc (Avg/Std): {6:.2f}/{7:.2f}".format(
                    fold + 1,
                    fold_loss_avg, fold_loss_std, 
                    fold_performance_avg.get(self.performance_method).get("average"),
                    fold_performance_avg.get(self.performance_method).get("std"),
                    validation_loss_avg, validation_loss_std, 
                    validation_performance.get(self.performance_method).get("average"),
                    validation_performance.get(self.performance_method).get("std")))

            elif (self.performance_method == "dice_coefficient"):

                print("-> Fold {0} Train_Loss (Avg/Std): {1:.2f}/{2:.2f} Train Dice Coeff(Avg/Std): {3:.2f}/{4:.2f}  Validation Loss (Avg/Std): {5:.2f}/{6:.2f} Validation Dice Coeff (Avg/Std): {7:.2f}/{8:.2f}".format(
                    fold + 1,
                    fold_loss_avg, fold_loss_std, 
                    fold_performance_avg.get(self.performance_method).get("average"),
                    fold_performance_avg.get(self.performance_method).get("std"),
                    validation_loss_avg, validation_loss_std, 
                    validation_performance.get(self.performance_method).get("average"),
                    validation_performance.get(self.performance_method).get("std")))

            print("-----------------------------------------------------------------------------------------")




        train_finish_time =  time.process_time() - train_start_time

        print("Model Training Time - {0}h {1}m {2:.2}s".format(int(train_finish_time/(60*60)), (train_finish_time % 3600) // 60, train_finish_time%60))
        





        self.__updateConfig(batch_size = batch_size, n_epochs = n_epochs, kFolds = KFolds,
                            function_loss = self.function_loss.loss_type, 
                            optimizer_name = "Adam", optimizer_learningRate = 0.001, 
                            trainingTime = "{0}h {1}m {2:.2}s".format(int(train_finish_time/(60*60)), (train_finish_time % 3600) // 60, train_finish_time%60),
                            trainingTime_epoch = trainingTime_epoch,
                            train_results_batch = self.statistics_train_batch, train_results_epoch = self.statistics_train_epoch,
                            train_results_fold = self.statistics_train_fold,
                            validation_results_fold = self.statistics_validation_fold)
        
        #TODO change adam aboce to a more dynamic approach

        self.model_trained = True
        
        print("Model Training: Complete")

        


    def __train_batch(self, data):



        
        def intersection_over_union(y_pred, y_true):
            y_pred = y_pred > 0.5  # binarize predictions
            y_true = y_true > 0.5  # binarize true masks
            y_pred = y_pred.to(torch.long)  # convert to long data type
            y_true = y_true.to(torch.long)  # convert to long data type
            intersection = torch.logical_and(y_pred, y_true).sum(dim=[2, 3])  # sum over height and width dimensions
            union = torch.logical_or(y_pred, y_true).sum(dim=[2, 3])  # sum over height and width dimensions
            iou = intersection.float() / union.float()
            return iou.mean(dim=[0, 1])  # average over batch and channel dimensions



        def accuracy (y_pred, label):
            acc = (y_pred == label).sum().item()/len(y_pred)
            return acc
                
        if (self.problem_type == "supervised" and
            self.problem == "classification"):
            
        
            # get the inputs; data is a list of [inputs, labels]
            inputs, labels = data

            inputs = inputs.to(self.device)
            labels = labels.to(self.device)
        
            # track history if only in train
            with torch.set_grad_enabled(True):
                
                # 1. Forward pass
                # forward + backward + optimize
                '''
                if(self.model.baseModel is None):
                    y_pred = self.model.forward(inputs)
                else:
                    y_pred = self.model.baseModel(inputs)
                '''
                y_pred = self.model_forward(inputs)
                                
                # 2. Calculate  and accumulate loss
                loss = self.function_loss(y_pred, labels)
                
                # 3. Optimizer zero grad
                self.optimizer.zero_grad()
                
                # 4. Loss backward
                loss.backward()
                
                # 5. Optimizer step
                self.optimizer.step()
            # Calculate and accumulate accuracy metric across all batches
            y_pred_class = torch.argmax(torch.softmax(y_pred, dim=1), dim=1)
            
            #y_pred_class = torch.argmax(y_pred, dim=1)
            acc = (y_pred_class == labels).sum().item()/len(y_pred)
        
            print("Train Batch: Loss: {0} Acc: {1}}]".format(loss.item(), acc))
            return loss.item(), acc
        
        
        if (self.problem_type == "supervised" and
            self.problem == "segmentation"):
            
            # get the inputs; data is a list of [inputs, labels]
            #data_tqdm = tqdm.tqdm(data)
            #inputs, masks = data_tqdm
            inputs, masks, *other = data

            #masks = (masks > 1).float()
            #print("Inputs: {}".format(len(inputs)))
            #print(inputs[0])
            #print(inputs[1])
            #print(inputs[2])
            #print("masks: {}".format(len(masks)))
            #print(masks[0])
            #inputs = inputs.to(self.device)
            #masks = masks.to(self.device)
            print("Inputs Type {0} Mask Type {1}".format(type(inputs), type(masks)))
            print("Inputs Shape {0} Mask Shape {1}".format(inputs.size(), masks.size()))
            print("Inputs DEVICE {0} Mask Device {1}".format(inputs.device, masks.device))
            
            
            #print("inputs: {0}".format(inputs.shape))
            #print("masks: {0}".format(masks.shape))
            #print("mask 1: {0}".format(masks[0]))
          
        
            # track history if only in train
            with torch.set_grad_enabled(True):
                
                # 1. Forward pass
                # forward + backward + optimize
                '''
                if(self.model.baseModel is None):
                    y_pred = self.model.forward(inputs)
                else:
                    y_pred = self.model.baseModel(inputs)
                '''
                y_pred = self.model_forward(inputs)
                #print("Y_pred: {0}".format(y_pred.shape))
                #print("Y_pred: {0}".format(y_pred))
                
                # 2. Calculate  and accumulate loss
                loss = self.function_loss(y_pred, masks)
                
                # 3. Optimizer zero grad
                self.optimizer.zero_grad()
                
                # 4. Loss backward
                loss.backward()
                
                # 5. Optimizer step
                self.optimizer.step()
            
            #y_pred_class = torch.argmax(y_pred, dim=1)
            iou = intersection_over_union(y_pred, masks)
            return loss.item(), iou


    def __train_batch_mixed_precision(self, performance_method, data, scaler):

        
        # forward
        with torch.cuda.amp.autocast():

            inputs, masks, *other = data

            y_pred = self.model_forward(inputs)

            loss, avg_loss, std_loss = self.function_loss.forward(y_pred = y_pred, y_true = masks)
            
            loss_index_max = torch.argmax(loss)
            loss_index_min = torch.argmin(loss)
            loss_index_random = torch.randint(0, len(loss), (1,)).item()

            if (avg_loss > self.worstSample_loss):
                self.worstSample[0] = inputs[loss_index_max]
                self.worstSample[1] = masks[loss_index_max]
                self.worstSample[2] = torch.sigmoid(y_pred[loss_index_max])
                self.worstSample_loss = loss[loss_index_max]

            elif (avg_loss < self.bestSample_loss):
                self.bestSample[0] = inputs[loss_index_min]
                self.bestSample[1] = masks[loss_index_min]
                self.bestSample[2] = torch.sigmoid(y_pred[loss_index_min])
                self.bestSample_loss = loss[loss_index_min]
            

            self.randomSample[0] = inputs[loss_index_random]
            self.randomSample[1] = masks[loss_index_random]
            self.randomSample[2] = torch.sigmoid(y_pred[loss_index_random])
            self.randomSample_loss = loss[loss_index_random]


            performance = self.__check_performance()

            if(torch.isnan(loss).any()):
                print("Train Batch Loss has NaN values")

            # backward
            self.optimizer.zero_grad()
            scaler.scale(avg_loss).backward()




            min_norm = 1e-7
            max_norm = 1

            # Unscales the gradients of optimizer's assigned params in-place
            scaler.unscale_(self.optimizer)

            # clip gradients
            #torch.nn.utils.clip_grad_value_(self.model.parameters(), clip_value=max_norm)
            torch.nn.utils.clip_grad_norm_(self.model.parameters(), max_norm)

            # Clip the gradients that are too small
            # If the total norm is less than the minimum norm, scale the gradients to the minimum norm
            '''
            for p in self.model.parameters():
                if p.grad is not None:
                    p.grad = torch.max(p.grad, torch.ones_like(p.grad) * min_norm)
            '''

            '''
            for param in self.model.parameters():
                if (param.grad is not None and param.requires_grad):

                    grad_norm = torch.norm(param.grad)

                    if grad_norm <= min_norm:
                
                        clipped_norm = torch.clamp(grad_norm, min_norm)

                        # scale the gradients to the new norm
                        param.grad *= clipped_norm / grad_norm
                else:
                    print("Parameter: {} is None".format(param))
            '''


                    
            scaler.step(self.optimizer)
            scaler.update()

        return avg_loss.item(), std_loss.item(), performance

                    
    def __train_epoch(self, kFold, epoch, performance_method, data, scaler, data_number_total):

        epoch_start_time = time.process_time()
        epoch_loss_avg = 0.0
        epoch_loss_std = 0.0
        epoch_performance = {}
        
        data_tqdm = tqdm.tqdm(data) #Nice Progress Bar
        

        i = 0
        for batch_i, data_batch in enumerate(data_tqdm, 1):


            data_number_total += self.batch_size

            data_batch = [d.to(self.device) for d in data_batch]
            avg_loss, std_loss, performance = self.__train_batch_mixed_precision(performance_method = performance_method,
                                                                        data = data_batch, scaler = scaler)



            # update tqdm loop
            if (self.data_type == "image"):
                if (self.problem == "segmentation"):

                    data_tqdm.set_postfix(Loss_Avg = avg_loss, Loss_Std = std_loss, Accuracy_Avg = performance.get("accuracy_pixel").get("average"), IoU_Avg = 1-performance.get("iou").get("average"))


            
            
            self.statistics_train_batch.append([data_number_total, performance])
            

            epoch_loss_avg += avg_loss
            epoch_loss_std += std_loss
            self.__sum_dictionaries(epoch_performance, performance)

            i += 1


        # Adjust metrics to get average loss and performance per batch
        
        epoch_loss_avg = epoch_loss_avg / i
        epoch_loss_std = epoch_loss_std / i
        self.__division_dict_float (epoch_performance, i)
        epoch_performance_avg = epoch_performance


        
        return time.process_time() - epoch_start_time, epoch_loss_avg, epoch_loss_std, epoch_performance_avg




    #Test Model Section
    ########################################################################################


    def model_test(self, performance_method, data_test = None, cv = False, n_samples = 20):
    
        if(data_test == None):

            if (self.data_test != None):
                data_test = torch.utils.data.DataLoader(self.data_test, batch_size = self.batch_size)
            else:
                print("ERROR -> Model Evaluation: Data Test is None")
                return
        else:
            #TODO DATALOADER
            pass

        if (cv == False):
            print("Loss FUnction: {0}".format(self.function_loss.loss_type))

        self.model.eval()

        

        testing_loss_avg = 0.0
        testing_loss_std = 0.0
        testing_performance = {}

        with torch.no_grad():

            i = 0
            data_tqdm = tqdm.tqdm(data_test) #Nice Progress Bar

            for batch_i, data_batch in enumerate(data_tqdm, 1):

                data_batch = [d.to(self.device) for d in data_batch]

                if (self.problem_type == "supervised"):

                    if (self.problem == "segmentation"):

                        X, Y = data_batch

                        Y_pred = self.model_forward(X)

                        loss, avg_loss, std_loss = self.function_loss.forward(y_pred = Y_pred.float(), y_true = Y.float())
                    
                        performance = self.__check_performance()

                    elif (self.problem == "classification"):
                        '''
                        images,labels = images.to(device),labels.to(device)
                        output = model(images)
                        loss=loss_fn(output,labels)
                        valid_loss+=loss.item()*images.size(0)
                        scores, predictions = torch.max(output.data,1)
                        val_correct+=(predictions == labels).sum().item()
                        '''
                        pass


                # update tqdm loop
                if(self.data_type == "image"):
                    if (self.problem == "segmentation"):
                        
                        data_tqdm.set_postfix(Loss_Avg = avg_loss.item(), Loss_Std = std_loss.item(), Accuracy_Avg = performance.get("accuracy_pixel").get("average"), IoU_Avg = 1-performance.get("iou").get("average"))





                testing_loss_avg += avg_loss.item()
                testing_loss_std += std_loss.item()
                self.__sum_dictionaries(testing_performance, performance)

                i += 1


        
        testing_loss_avg = testing_loss_avg / i
        testing_loss_std = testing_loss_std / i
        self.__division_dict_float(testing_performance, i)


        if (cv == False): # testing mode
            self.statistics_test = [testing_loss_avg, testing_loss_std, testing_performance]
            self.__updateConfig(test_results = [testing_loss_avg, testing_loss_std, testing_performance])



        return testing_loss_avg, testing_loss_std, testing_performance



    def __check_performance(self):



        performance = None

        if(self.problem_type == "supervised"):

            # Must have y_pred and y_true

            if (self.problem == "segmentation" and self.data_type == "image"):

                # y are masks


                # accuracy pixels
                #########################################################      

                accuracy_pixel_average = 1.00 - self.function_loss.loss_dict.get("accuracy_pixel").get("average").item()
                accuracy_pixel_std = self.function_loss.loss_dict.get("accuracy_pixel").get("std").item()

                # dice coefficient
                #########################################################
                

                dice_average = self.function_loss.loss_dict.get("dice_coefficient").get("average").item()
                dice_std = self.function_loss.loss_dict.get("dice_coefficient").get("std").item()

                #iou
                #########################################################

                iou_average = self.function_loss.loss_dict.get("iou").get("average").item()
                iou_std = self.function_loss.loss_dict.get("iou").get("std").item()


                #binary cross entropy
                #########################################################

                bce_average = self.function_loss.loss_dict.get("binary_cross_entropy").get("average").item()
                bce_std = self.function_loss.loss_dict.get("binary_cross_entropy").get("std").item()

                #focal loss
                #########################################################

                focal_average = self.function_loss.loss_dict.get("focal").get("average").item()
                focal_std = self.function_loss.loss_dict.get("focal").get("std").item()


                performance = {

                    "accuracy_pixel": {
                        "average": accuracy_pixel_average,
                        "std": accuracy_pixel_std,
                    },
                    "dice_coefficient": {
                        "average": dice_average,
                        "std": dice_std,
                    },
                    "iou": {
                        "average": iou_average,
                        "std": iou_std,
                    },
                    "binary_cross_entropy": {
                        "average": bce_average,
                        "std": bce_std,
                    },
                    "focal": {
                        "average": focal_average,
                        "std": focal_std,
                    },
                    
                }


                return performance

            else:
                print("X ERROR: Check Performance")

        else:
            print("X ERROR: Check Performance")

    

    # Others
    ########################################################################################

    def __tensorToPil(self, tensor):

        transform = torchvision.transforms.ToPILImage()

        return transform(tensor)

    def __PILToTensor(self, image):
    

        return torchvision.transforms.functional.pil_to_tensor(image)


    def __sum_dictionaries(self, dict1, dict2):

        for key in dict2:

            if (isinstance(dict1.get(key), dict)):
                self.__sum_dictionaries(dict1.get(key), dict2.get(key))
            else:
                if key in dict1:
                    dict1[key] += dict2[key]
                else:
                    dict1[key] = dict2[key]
        
        return dict1


    def __division_dict_float(self, dict1, number):

        for key in dict1:
            if (isinstance(dict1.get(key), dict)):
                self.__division_dict_float(dict1.get(key), number)
            else:   
                dict1[key] /= number


    def __updateConfig(self, loadingTime = None, transformations = None, train_size = None, test_size = None, test_factor = None,
                        model_name = None, model_parameters = None, 
                        image_input_size = None, image_input_channels = None, image_output_size = None, image_output_channels = None,
                        batch_size = None, n_epochs = None, kFolds = None, function_loss = None, 
                        optimizer_name = None, optimizer_learningRate = None, optimizer_momentum = None, trainingTime = None, trainingTime_epoch = None,
                        train_results_batch = None, train_results_epoch = None, train_results_fold = None, validation_results_fold = None, test_results = None,
                        problem = None, problem_type = None, data_type = None, device = None, version = None):


        if (loadingTime != None):
            self.config.get("data").update({"loadingTime": loadingTime}) # string
        
        if (transformations != None):
            self.config.get("data").update({"transformations": transformations}) # list
        
        if (train_size != None):
            self.config.get("data").update({"train_size": train_size}) # int

        if (test_size != None):
            self.config.get("data").update({"test_size": test_size}) # int
        
        if (test_factor != None):
            self.config.get("data").update({"test_factor": test_factor}) # float



        
        if (model_name != None):
            self.config.get("model").update({"name": model_name}) # string

        if (image_input_size != None):
            self.config.get("model").get("input").update({"image_size": image_input_size})
        
        if (image_input_channels != None):
            self.config.get("model").get("input").update({"image_channels": image_input_channels})
        
        if (image_output_size != None):
            self.config.get("model").get("output").update({"image_size": image_output_size})
        
        if (image_output_channels != None):
            self.config.get("model").get("output").update({"image_channels": image_output_channels})
    

        
        if (batch_size != None):
            self.config.get("train").update({"batch_size": batch_size})
        
        if (n_epochs != None):
            self.config.get("train").update({"n_epochs": n_epochs})
        
        if (kFolds != None):
            self.config.get("train").update({"kFolds": kFolds})
        
        if (function_loss != None):
            self.config.get("train").update({"function_loss": function_loss})
        
        if (optimizer_name != None):
            self.config.get("train").get("optimizer").update({"name": optimizer_name})

        if (optimizer_learningRate != None):
            self.config.get("train").get("optimizer").update({"learningRate": optimizer_learningRate})
        
        if (optimizer_momentum != None):
            self.config.get("train").get("optimizer").update({"momentum": optimizer_momentum})
        
        if (trainingTime != None):
            self.config.get("train").update({"trainingTime": trainingTime})

        if (trainingTime_epoch != None):
            self.config.get("train").update({"trainingTime_epoch": trainingTime_epoch})




        if (train_results_batch != None):
            self.config.get("statistics").update({"train_results_batch": train_results_batch})
        
        if (train_results_epoch != None):
            self.config.get("statistics").update({"train_results_epoch": train_results_epoch})
        
        if (train_results_fold != None):
            self.config.get("statistics").update({"train_results_fold": train_results_fold})
        
        if (validation_results_fold != None):
            self.config.get("statistics").update({"validation_results_fold": validation_results_fold})
        
        if (test_results != None):
            self.config.get("statistics").update({"test_results": test_results})
        



        if (problem != None):
            self.config.get("others").update({"problem": problem})

        if (problem_type != None):
            self.config.get("others").update({"problem_type": problem_type})

        if (data_type != None):
            self.config.get("others").update({"data_type": data_type})

        if (device != None):
            self.config.get("others").update({"device": device})


    def saveConfig(self, newConfig = False):

        print("Saving Iteration: Initialization")



        # convert dictionary to JSON string
        json_string = json.dumps(self.config)


        if (False == True):
            self.__setupDirectory()


        # open a file in write mode
        #config_parameters_directory = os.path.join(self.configDirectory, "self_parameters.json")

        #if (os.path.exists(config_parameters_directory) == False):
        #    os.makedirs(config_parameters_directory)


        #if (os.path.isdir(directory) == False):
        #    print("{0} is not a Directory".format(directory))

        with open(self.configDirectory + "/" + "self_parameters.json", "w") as outfile:

            # write the JSON string to the file
            outfile.write(json_string)

        # close the file
        outfile.close()



        #model_parameters_directory = os.path.join(self.configDirectory, "model.pth")

        #if (os.path.exists(model_parameters_directory) == False):
        #    os.makedirs(model_parameters_directory)


        torch.save(self.model.state_dict(), self.configDirectory + "/" + "model.pth")

        print("Saving Iteration: Complete")
        
        

    